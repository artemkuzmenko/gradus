<?php
/**
 * Template name: Главная
 */

get_header('main');
?>

<section class="main-page-baner">
        <div class="container baner-bg pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-8 col-sm-9 col-9">
                    <div class="baner-info-block">
                    <?php if($iq_gradus_options ['iq_gradus_baner_title']){ ?>
                        <h1 class="baner-title">
                        <?php echo esc_attr($iq_gradus_options['iq_gradus_baner_title']); ?>
                        </h1>
                        <?php } ?>
                        <?php if($iq_gradus_options ['iq_gradus_baner_text']){ ?>
                        <p class="baner-description">
                        <?php echo esc_attr($iq_gradus_options['iq_gradus_baner_text']); ?>
                        </p>
                        <?php } ?>
                        <?php if($iq_gradus_options ['iq_gradus_baner_link']){ ?>
                        <div class="baner-button-wrapper">
                            <a href="<?php echo esc_url($iq_gradus_options['iq_gradus_baner_link']); ?>" class="solid-button">Выбрать игру</a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-4 col-sm-3 col-3 paddings">
                    <div class="baner-image-block">
                        <img src="<?php echo get_template_directory_uri() ?>/img/main/N-book.png" class="baner-img" alt="baner image">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cool-products">
        <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
            <div class="row">
            <?php
            $loop = new WP_Query( array(
            'post_type' => 'product',
            'posts_per_page' => '2',
             'orderby' => 'date',
             'product_cat' => 'best',
                ));
                while ( $loop->have_posts() ): $loop->the_post(); ?>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="cool-product-card">
                        <div class="cool-product-img"> 
                            <a href="<?php the_permalink(); ?>">
                            <?php echo get_the_post_thumbnail(get_the_ID(), 'cool_product_front'); ?>
                        </a>
                        </div>
                        <div class="cool-product-info">
                            <h3 class="cool-product-title"><a class="product-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <p class="cool-product-descr">
                                <a class="product-link" href="<?php the_permalink(); ?>">
                                <?php echo get_the_excerpt() ?>
                            </a>
                            </p>
                            <div class="cool-product-price">
                            <?php woocommerce_template_loop_price(); ?>
                            </div>
                            <div class="basket-and-like-wrapper">
                            <?php woocommerce_template_loop_add_to_cart(); ?>
                            <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id="'.get_the_ID().'" variation_id="0"]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; wp_reset_postdata(); ?>
            </div>
        </div>
    </section>

    <section class="products-slider">
        <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="slider-info-wrapper">
                        <div class="slider-title">
                            Популярная серия игр
                        </div>
                        <?php if($iq_gradus_options ['iq_gradus_baner_link']){ ?>
                        <div class="show-all-slider hide-on-mobile">
                            <a href="<?php echo esc_url($iq_gradus_options['iq_gradus_baner_link']); ?>" class="show-all-slider-link link-hover">Смотреть все</a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="main-page-slider">
                <!-- Slide start -->
                <?php
            $slider = new WP_Query( array(
            'post_type' => 'product',
            'posts_per_page' => '5',
             'orderby' => 'date',
                ));
                while ( $slider->have_posts() ): $slider->the_post(); ?>
                <div class="slide-wrapper">
                    <div class="product-card">
                        <div class="product-img">
                        <a href="<?php the_permalink(); ?>">
                        <?php if( get_field('woo_custom_image_in_slider') ): ?>
                        <img src="<?php the_field('woo_custom_image_in_slider'); ?>" />
                        <?php endif; ?>
                        </a>
                        </div>
                        <div class="product-card-info">
                            <h3 class="product-title"><a class="product-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <div class="product-card-description">
                            <a class="product-link" href="<?php the_permalink(); ?>">
                                <?php echo get_the_excerpt() ?>
                            </a>
                            </div>
                            <div class="product-price">
                            <?php woocommerce_template_loop_price(); ?>
                            </div>
                            <div class="basket-and-like-wrapper product-card-basket">
                            <?php woocommerce_template_loop_add_to_cart(); ?>
                            <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id="'.get_the_ID().'" variation_id="0"]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- / Slide end-->
                <?php endwhile; wp_reset_postdata(); ?>
            </div>
            <!-- /. Main page slider -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 hide-on-pc">
                <?php if($iq_gradus_options ['iq_gradus_baner_link']){ ?>
                    <div class="slider-info-wrapper">
                        <div class="show-all-slider">
                            <a href="<?php echo esc_url($iq_gradus_options['iq_gradus_baner_link']); ?>" class="show-all-slider-link link-hover">Смотреть все</a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- / Slider section end-->

    <?php get_template_part('template-parts/subscribe-capitan'); ?>

<?php get_footer(); ?>
