<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package iq-gradus
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
<div class="row">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</div>
</div><!-- #secondary -->

