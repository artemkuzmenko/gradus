<?php
/**
 * Template name: О нас
 */

get_header();
?>

<?php get_template_part('template-parts/page-title'); ?>

<section class="about">
    <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
        <div class="row">
            <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2">
                <div class="about-info-block">
                <?php if($iq_gradus_options ['iq_gradus_about_title']){ ?>
                    <h2 class="about-block-title"><?php echo esc_attr($iq_gradus_options['iq_gradus_about_title']); ?></h2>
                <?php } ?>
                <?php if($iq_gradus_options ['iq_gradus_about_text']){ ?>
                    <p class="about-description">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_about_text']); ?>
                    </p>
                    <?php } ?>
                    <?php if($iq_gradus_options ['iq_gradus_about_link']){ ?>
                    <div class="about-btn-wrapper">
                        <a class="solid-button" href="<?php echo esc_url($iq_gradus_options['iq_gradus_about_link']); ?>">Выбрать игру</a>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 order-xl-2 order-lg-2 order-md-1 order-sm-1 order-1">
                <div class="about-logo-wrapper">
                    <img class="about-logo" src="<?php echo get_template_directory_uri() ?>/img/about/Vector.svg" alt="logo">
                </div>
                <div class="about-all-heroes-wrapper">
                    <img class="about-all-heroes-img" src="<?php echo get_template_directory_uri() ?>/img/about/vsya_tolpa.png" alt="all">
                </div>
            </div>
        </div>
    </div>
</section>
 <!-- / About main block-->

 <section class="about-numbers">
    <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
        <div class="row">
        <?php if($iq_gradus_options ['iq_gradus_number_1']){ ?>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="number-block">
                    <div class="number-wrapper">
                        <div class="inside-number-wrapper">
                            1
                        </div>
                    </div>
                    <p class="number-text">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_number_1']); ?>
                    </p>
                </div>
            </div>
            <?php } ?>
            <?php if($iq_gradus_options ['iq_gradus_number_2']){ ?>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="number-block">
                <div class="number-wrapper">
                        <div class="inside-number-wrapper">
                            2
                        </div>
                    </div>
                    <p class="number-text">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_number_2']); ?>
                    </p>
                </div>
            </div>
            <?php } ?>
            <?php if($iq_gradus_options ['iq_gradus_number_3']){ ?>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="number-block">
                <div class="number-wrapper">
                        <div class="inside-number-wrapper">
                            3
                        </div>
                    </div>
                    <p class="number-text">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_number_3']); ?>
                    </p>
                </div>
            </div>
            <?php } ?>
            <?php if($iq_gradus_options ['iq_gradus_number_4']){ ?>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="number-block">
                <div class="number-wrapper">
                        <div class="inside-number-wrapper">
                            4
                        </div>
                    </div>
                    <p class="number-text">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_number_4']); ?>
                    </p>
                </div>
            </div>
            <?php } ?>
            <?php if($iq_gradus_options ['iq_gradus_number_5']){ ?>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="number-block">
                <div class="number-wrapper">
                        <div class="inside-number-wrapper">
                            5
                        </div>
                    </div>
                    <p class="number-text">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_number_5']); ?>
                    </p>
                </div>
            </div>
            <?php } ?>
            <?php if($iq_gradus_options ['iq_gradus_number_6']){ ?>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="number-block">
                <div class="number-wrapper">
                        <div class="inside-number-wrapper">
                            6
                        </div>
                    </div>
                    <p class="number-text">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_number_6']); ?>
                    </p>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
 <!-- / About numbers-->

 <section class="questions">
    <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="eg-wrapper">
                    <img src="<?php echo get_template_directory_uri() ?>/img/heroes/eg.png" alt="eg">
                </div>
            </div>
            <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 d-flex align-items-center">
                <div class="about-questions-wrapper">
                    <p class="about-questions-text"> 
                        Остались вопросы? В <a class="green-link link-hover" href="<?php echo esc_url($iq_gradus_options['iq_gradus_faq_page-link']); ?>">FAQ</a> собраны ответы на часто задаваемые вопросы.
                        Узнать подробнее об особенностях, ходе и правилах игры тебе поможет  <a class="green-link link-hover" href="<?php echo esc_url($iq_gradus_options['iq_gradus_game_instructions_link']); ?>">руководство к игре.</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('template-parts/subscribe-capitan'); ?>

<?php get_footer(); ?>