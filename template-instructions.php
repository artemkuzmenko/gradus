
<?php
/**
 * Template name: Руководство к игре
 */

get_header();
?>

<?php get_template_part('template-parts/page-title'); ?>

<section class="game-instructions">
    <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                <div class="tab">
                    <button class="tablinks" onclick="openTab(event, 'Features')" id="defaultOpen">Особенности игры</button>
                    <button class="tablinks" onclick="openTab(event, 'Game')">Ход игры</button>
                    <button class="tablinks" onclick="openTab(event, 'Rules')">Правила игры</button>
                  </div>
            </div>
            <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                <div id="Features" class="tabcontent">
                    <div class="all-heroes-wrapper">
                        <img class="all-heroes-img" src="<?php echo get_template_directory_uri() ?>/img/game-instructions/All-heroes.jpg" alt="all heroes">
                    </div>
                    <?php if($iq_gradus_options ['iq_gradus_game_features_1']){ ?>
                    <div class="tabs-info-block">
                        <div class="icon-wrapper">
                            <img src="<?php echo get_template_directory_uri() ?>/img/game-instructions/Camera.svg" alt="Camera">
                        </div>
                        <div class="info-block-text">
                            <p>
                            <?php echo esc_attr($iq_gradus_options['iq_gradus_game_features_1']); ?>
                            </p>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if($iq_gradus_options ['iq_gradus_game_features_2']){ ?>
                    <div class="tabs-info-block">
                        <div class="icon-wrapper">
                            <img src="<?php echo get_template_directory_uri() ?>/img/game-instructions/Browsers.svg" alt="Browsers">
                        </div>
                        <div class="info-block-text">
                            <p>
                            <?php echo esc_attr($iq_gradus_options['iq_gradus_game_features_2']); ?>
                            </p>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if($iq_gradus_options ['iq_gradus_game_features_3']){ ?>
                    <div class="tabs-info-block">
                        <div class="icon-wrapper">
                            <img src="<?php echo get_template_directory_uri() ?>/img/game-instructions/Plays.svg" alt="Plays">
                        </div>
                        <div class="info-block-text">
                            <p>
                            <?php echo esc_attr($iq_gradus_options['iq_gradus_game_features_3']); ?>
                            </p>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if($iq_gradus_options ['iq_gradus_game_features_4']){ ?>
                    <div class="tabs-info-block">
                        <div class="icon-wrapper">
                            <img src="<?php echo get_template_directory_uri() ?>/img/game-instructions/Friends.svg" alt="Friends">
                        </div>
                        <div class="info-block-text">
                            <p>
                            <?php echo esc_attr($iq_gradus_options['iq_gradus_game_features_4']); ?>
                            </p>
                        </div>
                    </div>
                    <?php } ?>
                  </div>
                  <!-- /.tab features -->
                  <div id="Game" class="tabcontent">
                    <div class="all-heroes-wrapper">
                        <img class="all-heroes-img" src="<?php echo get_template_directory_uri() ?>/img/game-instructions/All-heroes.jpg" alt="all heroes">
                    </div>
                    <?php if($iq_gradus_options ['iq_gradus_game_play_description']){ ?>
                    <div class="game-play">
                        <p>
                        <?php echo esc_attr($iq_gradus_options['iq_gradus_game_play_description']); ?>
                        </p>
                    </div>
                    <?php } ?>
                    <?php if($iq_gradus_options ['iq_gradus_game_play_description_title_1']){ ?>
                    <div class="gameplay-block">
                        <div class="gameplay-img-wrapper-right">
                            <img src="<?php echo get_template_directory_uri() ?>/img/game-instructions/step-1.png" alt="step-1">
                        </div>
                        <div class="gameplay-text">
                            <h5 class="gameplay-title"><?php echo esc_attr($iq_gradus_options['iq_gradus_game_play_description_title_1']); ?></h5>
                            <p class="gameplay-p">
                            <?php echo esc_attr($iq_gradus_options['iq_gradus_game_play_description_1']); ?>
                            </p>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if($iq_gradus_options ['iq_gradus_game_play_description_title_2']){ ?>
                    <div class="gameplay-block">
                        <div class="gameplay-text gameplay-text-left">
                            <h5 class="gameplay-title"><?php echo esc_attr($iq_gradus_options['iq_gradus_game_play_description_title_2']); ?></h5>
                            <p class="gameplay-p">
                            <?php echo esc_attr($iq_gradus_options['iq_gradus_game_play_description_2']); ?>
                            </p>
                        </div>
                        <div class="gameplay-img-wrapper-left">
                            <img src="<?php echo get_template_directory_uri() ?>/img/game-instructions/step-2.png" alt="step-2">
                        </div>
                    </div>
                    <?php } ?>
                    <?php if($iq_gradus_options ['iq_gradus_game_play_description_title_3']){ ?>  
                    <div class="gameplay-block">
                        <div class="gameplay-img-wrapper-right">
                            <img src="<?php echo get_template_directory_uri() ?>/img/game-instructions/step-3.png" alt="step-3">
                        </div>
                        <div class="gameplay-text">
                            <h5 class="gameplay-title"><?php echo esc_attr($iq_gradus_options['iq_gradus_game_play_description_title_3']); ?></h5>
                            <p class="gameplay-p">
                            <?php echo esc_attr($iq_gradus_options['iq_gradus_game_play_description_3']); ?> 
                        </p>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if($iq_gradus_options ['iq_gradus_game_play_description_title_4']){ ?>  
                    <div class="gameplay-block gameplay-block-mb">
                        <div class="gameplay-text gameplay-text-left">
                            <h5 class="gameplay-title"><?php echo esc_attr($iq_gradus_options['iq_gradus_game_play_description_title_4']); ?></h5>
                            <p class="gameplay-p">
                            <?php echo esc_attr($iq_gradus_options['iq_gradus_game_play_description_4']); ?> 
                            </p>
                                <div class="gameplay-button-wrapper">
                                    <a href="<?php echo esc_url($iq_gradus_options['iq_gradus_game_play_link']); ?> " class="solid-button">Выбрать игру</a>
                                </div>
                        </div>
                        <div class="gameplay-img-wrapper-left">
                            <img src="<?php echo get_template_directory_uri() ?>/img/game-instructions/step-4.png" alt="step-4">
                        </div>
                    </div>
                    <?php } ?>
                  </div>
                  <!-- /.game -->
                  <div id="Rules" class="tabcontent">
                    <div class="all-heroes-wrapper">
                        <img class="all-heroes-img" src="<?php echo get_template_directory_uri() ?>/img/game-instructions/All-heroes.jpg" alt="all heroes">
                    </div>
                    <?php if($iq_gradus_options ['iq_gradus_game_rules_title_1']){ ?>  
                    <div class="game-rules-block-wrapper">
                        <div class="game-rules-header-wrapper">
                            <h2 class="game-rules-header"><?php echo esc_attr($iq_gradus_options['iq_gradus_game_rules_title_1']); ?></h2>
                        </div>
                        <div class="game-rules-text-block">
                            <div class="game-rules-text-block-one">
                            <?php echo esc_attr($iq_gradus_options['iq_gradus_game_rules_description_1']); ?>
                            </div>
                            <div class="game-rules-text-block-two">
                            <?php echo esc_attr($iq_gradus_options['iq_gradus_game_rules_description_1_1']); ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if($iq_gradus_options ['iq_gradus_game_rules_title_2']){ ?>  
                    <div class="game-rules-block-wrapper">
                        <div class="game-rules-header-wrapper next-tour">
                            <h2 class="game-rules-header"><?php echo esc_attr($iq_gradus_options['iq_gradus_game_rules_title_2']); ?></h2>
                        </div>
                        <div class="game-rules-text-block">
                            <div class="game-rules-text-block-one">
                            <?php echo esc_attr($iq_gradus_options['iq_gradus_game_rules_description_2']); ?>
                            </div>
                            <div class="game-rules-text-block-two">
                            <?php echo esc_attr($iq_gradus_options['iq_gradus_game_rules_description_2_2']); ?>
                                </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if($iq_gradus_options ['iq_gradus_game_rules_title_3']){ ?>  
                    <div class="game-rules-block-wrapper">
                        <div class="game-rules-header-wrapper final-tour">
                            <h2 class="game-rules-header"><?php echo esc_attr($iq_gradus_options['iq_gradus_game_rules_title_3']); ?></h2>
                        </div>
                        <div class="game-rules-text-block">
                            <div class="game-rules-text-block-one"> <?php echo esc_attr($iq_gradus_options['iq_gradus_game_rules_description_3']); ?>
                        </div>
                            <div class="game-rules-text-block-two">
                            <?php echo esc_attr($iq_gradus_options['iq_gradus_game_rules_description_3_3']); ?>
                                </div>
                        </div>
                    </div>
                    <?php } ?>
                  </div>
                  <!-- /.tab rules -->
            </div>
    <!-- /.col-xl-3 end -->
        </div>
    </div>
</section>

<?php get_template_part('template-parts/subscribe-capitan'); ?>

<?php get_footer(); ?>