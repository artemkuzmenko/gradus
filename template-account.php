<?php
/**
 * Template name: Аккаунт
 */

get_header();
?>

<?php get_template_part('template-parts/page-title'); ?>

<section class="account-template-section">
<div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
<?php while ( have_posts() ) : the_post(); ?>
            <?php echo the_content(); ?>
        <?php endwhile; ?>
        </div> 
</div>
</section>

<?php get_template_part('template-parts/subscribe-capitan'); ?>

<?php get_footer(); ?>