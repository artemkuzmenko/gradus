<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package iq-gradus
 */

get_header();
?>

<?php get_template_part('template-parts/page-title'); ?>

<section class="base-template-section">
<div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
    <div class="row">
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php echo the_content(); ?>
        </div>
        <?php endwhile; ?>
    </div>
</div>
</section>

<?php get_template_part('template-parts/subscribe-capitan'); ?>
<?php get_footer(); ?>

