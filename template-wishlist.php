<?php
/**
 * Template name: Избранное
 */

get_header();
?>
<?php get_template_part('template-parts/page-title-wish'); ?>

<section class="base-template-section">
<div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
    <div class="row">
    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
<nav class="woocommerce-MyAccount-navigation">
	<ul>
					<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--downloads">
				<a href="<?php echo esc_url($iq_gradus_options['iq_gradus_my_games_link']); ?>">Мои игры</a>
			</li>
					<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--wishlist is-active">
				<a href="<?php echo esc_url($iq_gradus_options['iq_gradus_wishlist_link']); ?>">Избранное</a>
			</li>
					<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account">
				<a href="<?php echo esc_url($iq_gradus_options['iq_gradus_edit_account_link']); ?>">Личные данные</a>
			</li>
			</ul>
</nav>
</div>
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
            <?php echo the_content(); ?>
        </div>
        <?php endwhile; ?>
    </div>
</div>
</section>

<?php get_template_part('template-parts/subscribe-capitan'); ?>
<?php get_footer(); ?>