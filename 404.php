<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package iq-gradus
 */

get_header();
?>

<section class="page-title">
        <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
            <div class="row">
                <div class="col-12">
                    <div class="page-title-bg">
                        <h1 class="page-title-header">
                            404 
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- /.page title section end -->

<section class="404-content">
    <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="page-404-hero">
                    <img src="<?php echo get_template_directory_uri() ?>/img/heroes/too-be.png" alt="to bee or not to bee">
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="text-block-wrapper-404">
                    <div class="block-title-404">
                        404.
                    </div>
                    <div class="block-description-404">
                        Страница, которую вы ищете, не существует
                    </div>
                    <div class="btn-wrapper-404">
                        <a class="solid-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">На главную страницу</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('template-parts/subscribe-capitan'); ?>

<?php get_footer(); ?>
