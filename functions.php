<?php
/**
 * iq-gradus functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package iq-gradus
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'iq_gradus_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function iq_gradus_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on iq-gradus, use a find and replace
		 * to change 'iq-gradus' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'iq-gradus', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'main' => esc_html__( 'Главное меню', 'iq-gradus' ),
				'mobile' => esc_html__( 'Мобильное меню', 'iq-gradus' ),
				'footer-1' => esc_html__( 'Меню в подвале - главное', 'iq-gradus' ),
				'footer-2' => esc_html__( 'Меню в подвале - поддержка', 'iq-gradus' ),
				'account' => esc_html__( 'Меню аккаунта',  'iq-gradus' )
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'iq_gradus_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
		// Crop images for blog
        add_image_size('cool_product_front', 210, 210, true);
        add_image_size('slider_img', 210, 120, true);
	}
endif;
add_action( 'after_setup_theme', 'iq_gradus_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function iq_gradus_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'iq_gradus_content_width', 640 );
}
add_action( 'after_setup_theme', 'iq_gradus_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function iq_gradus_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'iq-gradus' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'iq-gradus' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'iq_gradus_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function iq_gradus_scripts() {
	wp_enqueue_style( 'iq-gradus-style', get_stylesheet_uri() );
	wp_enqueue_style( 'iq-gradus-main-style', get_template_directory_uri() . '/css/style.min.css', array(), '30.08.19', 'all' );
	wp_enqueue_style( 'iq-gradus-style-slick', get_template_directory_uri() . '/css/slick.min.css', array(), '1.0', false );
	wp_enqueue_style( 'iq-gradus-style-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '1.0', false );
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'iq-gradus-main-js', get_template_directory_uri() . '/js/main.js', array(), true );
	wp_enqueue_script( 'iq-gradus-bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array(), true );
	wp_enqueue_script( 'iq-gradus-slick-js', get_template_directory_uri() . '/slick/slick.min.js', array(), true );
	wp_enqueue_script( 'iq-gradus-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'iq-gradus-filter-js', get_template_directory_uri() . '/js/filter.js', array(), true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'iq_gradus_scripts' );

//Filter widget init
require get_template_directory() . '/inc/widgets/widget-filter.php';

function iq_gradus_init_widgets() {
	register_widget('Iq_Gradus_Filter_Widget');
}
add_action('widgets_init', 'iq_gradus_init_widgets');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Redux options
 */
require get_template_directory() . '/inc/redux-options.php';

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
	require get_template_directory() . '/woocommerce/inc/wc-functions-remove.php';
	require get_template_directory() . '/woocommerce/inc/wc-functions.php';
	require get_template_directory() . '/woocommerce/inc/wc-function-archive.php';
	require get_template_directory() . '/woocommerce/inc/wc-functions-card.php';
	require get_template_directory() . '/woocommerce/inc/wc-functions-single.php';
}

//Contact form 7 remove br tags 
add_filter( 'wpcf7_autop_or_not', '__return_false' );


//Woo shortcode
add_shortcode ('woo_cart_but', 'woo_cart_but' );
/**
 * Create Shortcode for WooCommerce Cart Menu Item
 */
function woo_cart_but() {
	ob_start();
 
        $cart_count = WC()->cart->cart_contents_count; // Set variable for cart item count
        $cart_url = wc_get_cart_url();  // Set Cart URL
  
        ?>
        <li><a class="menu-item cart-contents" href="<?php echo $cart_url; ?>" title="My Basket">
	    <?php
        if ( $cart_count > 0 ) {
       ?>
            <span class="cart-contents-count"><?php echo $cart_count; ?></span>
        <?php
        }
        ?>
        </a></li>
        <?php
	        
    return ob_get_clean();
 
}

//Ajax cart 
add_filter( 'woocommerce_add_to_cart_fragments', 'woo_cart_but_count' );
/**
 * Add AJAX Shortcode when cart contents update
 */
function woo_cart_but_count( $fragments ) {
 
    ob_start();
    
    $cart_count = WC()->cart->cart_contents_count;
    $cart_url = wc_get_cart_url();
    
    ?>
    <a class="cart-contents menu-item" href="<?php echo $cart_url; ?>" title="<?php _e( 'Посмотреть корзину' ); ?>">
	<?php
    if ( $cart_count > 0 ) {
        ?>
        <span class="cart-contents-count"><?php echo $cart_count; ?></span>
        <?php            
    }
        ?></a>
    <?php
 
    $fragments['a.cart-contents'] = ob_get_clean();
     
    return $fragments;
}

/**
 * Filter the except length to 10 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 6;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
    return '';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

//Currency symbol 
add_filter( 'woocommerce_currency_symbol', 'my_custom_currency_symbol' );
function my_custom_currency_symbol( $symbol ) {
    return 'руб.';
}

//Woo Filter by category
function iq_gradus_show_products(){
	
	$query_data = $_GET;
		
    $paged = (isset($query_data['paged']) ) ? intval($query_data['paged']) : 1;

    $orderby = 'date';
	if(!empty($query_data['order'])){
		 $order = $query_data['order'];
	}
	else{
		$order = 'ASC';
	}

    $posts_per_page = get_option('woocommerce_catalog_columns') * get_option('woocommerce_catalog_rows');

    //filter by category id
	$cats = ($query_data['category']) ? explode(',',$query_data['category']) : false;

    $tax_query_cat = ($cats) ? array( array(
        'taxonomy' => 'product_cat',
        'field' => 'id',
        'terms' => $cats
    ) ) : false;
 
    $args = array(
		'post_type' => 'product',
        'paged' => $paged,
        'posts_per_page' => $posts_per_page,
		'tax_query' => array(
			'relation' => 'AND',
			$tax_query_cat
		),
	);

    switch ( $orderby ) {
        case 'date':
            $args['orderby'] = 'date';
            $args['order'] = $order; 
            break;
    }

    $loop = new WP_Query( $args );
    if ( $loop->have_posts() ) {
		woocommerce_product_loop_start();
        while ( $loop->have_posts() ) : $loop->the_post();
            wc_get_template_part( 'content', 'product' );
		endwhile;
		woocommerce_product_loop_end();
?>
            <?php if($loop->max_num_pages > 1){ ?>
                <nav class="pagination-custom">
                    <div class="nav-links">
                        <?php
                        //Выводим левую стрелку для первой страницы
                        if( $paged == 0 or $paged == 1){ ?>
                            <span class="prev"></span>
                        <?php } ?>

                        <?php

                        //Вывод стандартной пагинации
                        $big = 999999999; // need an unlikely integer

                        echo paginate_links( array(
                            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                            'format' => '?paged=%#%',
                            'current' => max( 1, $paged ),
                            'prev_text'          => 'Предыдущая',
                            'next_text'          => 'Следующая',
                            'total' => $loop->max_num_pages
                        ) );
                        ?>

                        <?php
                        //Выводим правую стрелку для последней страницы
                        if( $paged == $loop->max_num_pages){ ?>
                            <span class="next"></span>
                        <?php } ?>
                    </div>
                </nav>
            <?php } ?>
  <?php
    } else {
        echo __( 'Игр не найдено','iq_gradus' );
    }
    wp_reset_postdata();


    die();
}

add_action('wp_ajax_iq_gradus_filter', 'iq_gradus_show_products');
add_action('wp_ajax_nopriv_iq_gradus_filter', 'iq_gradus_show_products');


//Account functions 
	function my_woocommerce_account_menu_items($items) {
    unset($items['dashboard']);         // убрать вкладку Консоль
    unset($items['orders']);             // убрать вкладку Заказы
    unset($items['edit-address']);         // убрать вкладку Адреса
    unset($items['customer-logout']);     // убрать вкладку Выйти
    return $items;
}

add_filter( 'woocommerce_account_menu_items', 'my_woocommerce_account_menu_items', 10 );

/* Edit endpoint names */
function my_account_menu_order() {
	$menuOrder = array(
		'downloads'          => __( 'Мои игры', 'woocommerce' ),
		'wishlist'          => __( 'Избранное', 'woocommerce' ),
		'edit-account'       => __( 'Личные данные', 'woocommerce' ),
	);
	return $menuOrder;
}
add_filter ( 'woocommerce_account_menu_items', 'my_account_menu_order' );


//Download product thumbnail
$product = wc_get_product( $product_id ); 
function my_account_downloads_column_download_product( $download ) {
    // Get $product object from product ID
    $product = wc_get_product( $download['product_id'] );
	
    // Get thumbnail
    $thumbnail = $product->get_image(array( 210, 210)); // Get the product thumbnail (from product object)
	
    // Image found
    if( $product->get_image_id() > 0 ) {
        $item_name = '<div class="product-img">' . $thumbnail . '</div>';
        echo $item_name;
    }
	echo '<h3 class="product-title">';
	echo '<a href="' . $product->get_permalink() . '">' . $product->get_name() . '</a>';
	echo '</h3>';
}
add_action( 'woocommerce_account_downloads_column_download-product', 'my_account_downloads_column_download_product' );

//Edit account form fields 
add_filter('woocommerce_save_account_details_required_fields', 'iq_gradus_myaccount_required_fields');
 
function iq_gradus_myaccount_required_fields( $account_fields ) {
	unset( $account_fields['account_last_name'] );
	unset( $account_fields['account_display_name'] );
	unset( $account_fields['account_first_name'] );
	return $required_fields; 
}

//Account order count
function display_woocommerce_order_count( $atts, $content = null ) {
	$args = shortcode_atts( array(
		'status' => 'completed',
	), $atts );
	$statuses    = array_map( 'trim', explode( ',', $args['status'] ) );
	$order_count = 0;
	foreach ( $statuses as $status ) {
		// if we didn't get a wc- prefix, add one
		if ( 0 !== strpos( $status, 'wc-' ) ) {
			$status = 'wc-' . $status;
		}
		$order_count += wp_count_posts( 'shop_order' )->$status;
	}
	ob_start();
	echo number_format( $order_count );
	return ob_get_clean();
}
add_shortcode( 'wc_order_count', 'display_woocommerce_order_count' );

//Account menu filter
add_filter( 'wp_nav_menu_items', 'add_loginout_link', 10, 2 );

function add_loginout_link( $items, $args ) {

   if (is_user_logged_in() && $args->theme_location == 'account') {
		   echo '<div class="iq-gradus-login iq-gradus-user-log-in">';
		   echo '<div class="iq-gradus-user-login-icon"></div>';
		   echo '<div class="iq-gradus-user-login-arrow"></div>';
		$user = wp_get_current_user();
		echo "<div id='header-welcome'>". $user->display_name ."</div>";
		$items .= '<li class="login-menu-link-wrapper"><a class=" login-menu-link login-menu-link-user" href="' . get_permalink( woocommerce_get_page_id( 'myaccount' ) ) . '">Личный кабинет</a></li>';
		$items .= '<li class="login-menu-link-wrapper login-menu-link-wrapper-exit"><a class="login-menu-link login-menu-link-exit" href="'. wp_logout_url( get_permalink( woocommerce_get_page_id( 'myaccount' ) ) ) .'">Выход</a></li>';	 
		echo '</div>';
   }
   elseif (!is_user_logged_in() && $args->theme_location == 'account') {
	echo '<div class="iq-gradus-login">';
	echo '<div class="iq-gradus-user-login-icon-not-active"></div>';
	   $items .= '<li class="login-menu-link-wrapper"><a class="login-menu-link login-menu-link-exit"  href="#" data-toggle="modal" data-target="#loginModal">Вход</a></li>';
	   echo '</div>';
   }
   return $items;
}

//Send info to admin 
add_action('woocommerce_created_customer', 'admin_email_on_registration', 10, 1);
function admin_email_on_registration( $customer_id ) {
  wp_new_user_notification( $customer_id );
}

//Woocommerce short description
add_filter('woocommerce_short_description', 'custom_woocommerce_short_description', 10, 1);
function custom_woocommerce_short_description($post_excerpt){
    if (!is_product()) {
        $post_excerpt = substr($post_excerpt, 0, 10);
    }
    return $post_excerpt;
}

