<?php
/**
 * Template name: Персонажи
 */

get_header();
?>

<?php get_template_part('template-parts/page-title'); ?>

<section class="all-heroes">
    <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
               <div class="all-heroes">
                   <div class="play-btn-wrapper">
                       <a class="link-hover play-btn-link" href="#" data-toggle="modal" data-target="#videoModal">Посмотрите правила игры</a>
                   </div>
                   <div class="myka-wrapper">
                    <a href="#ya1" onclick="openTab(event, 'Myka')" class="hero-name scroll-to">мука</a>
                </div>
                <div class="semchy-wrapper">
                    <a href="#ya2" onclick="openTab(event, 'Semchy')"  class="hero-name scroll-to" >семчу</a>
                </div>
                <div class="egen-wrapper">
                    <a href="#ya3" onclick="openTab(event, 'Eg')" class="hero-name scroll-to">ЕГЭн</a>
                </div>
                <div class="cap-wrapper">
                    <a href="#ya4" class="hero-name scroll-to" onclick="openTab(event, 'Cap')">КАПИ</a>
                </div>
                <div class="dance-wrapper">
                    <a href="#ya5" class="hero-name scroll-to" onclick="openTab(event, 'Dance')">ДАнц</a>
                </div>
                <div class="muzo-wrapper">
                    <a href="#ya6" class="hero-name scroll-to" onclick="openTab(event, 'Muzo')">музо</a>
                </div>
                <div class="tube-wrapper">
                    <a href="#ya7" class="hero-name scroll-to" onclick="openTab(event, 'Tube')">туби</a>
                </div>
                   <img src="<?php echo get_template_directory_uri() ?>/img/heroes/all.png" alt="all heroes">
               </div>
            </div>
        </div>
    </div>
</section>

<section class="all-heroes-info">
<div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
    <div class="row">
        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
            <div class="tab-heroes">
                <div class="hero-tab-wrapper">
                    <button class="tablinks hero-tab first-hero-tab" onclick="openTab(event, 'Myka')" id="defaultOpen">МУКА <span class="hero-icon-wrapper"><img class="hero-icon" src="<?php echo get_template_directory_uri() ?>/img/heroes/Muka-icon.png" alt="myka icon"></span></button>
                </div>
                <div class="hero-tab-wrapper">
                    <button class="tablinks hero-tab" onclick="openTab(event, 'Semchy')">семчу* <span class="hero-icon-wrapper"><img class="hero-icon" src="<?php echo get_template_directory_uri() ?>/img/heroes/Semchu-icon.png" alt="semchy icon"></span></button>
                </div>
                <div class="hero-tab-wrapper">
                    <button class="tablinks hero-tab third-hero-tab" onclick="openTab(event, 'Eg')">ЕГЭн <span class="hero-icon-wrapper"><img class="hero-icon" src="<?php echo get_template_directory_uri() ?>/img/heroes/Egen-icon.png" alt="eg icon"></span></button>
                </div>
                    <div class="hero-tab-wrapper">
                        <button class="tablinks hero-tab" onclick="openTab(event, 'Cap')">КАПИ <span class="hero-icon-wrapper"><img class="hero-icon" src="<?php echo get_template_directory_uri() ?>/img/heroes/Capi-icon.png" alt="cap icon"></span></button>
                    </div>
                    <div class="hero-tab-wrapper">
                        <button class="tablinks hero-tab" onclick="openTab(event, 'Dance')">ДАнц <span class="hero-icon-wrapper"><img class="hero-icon" src="<?php echo get_template_directory_uri() ?>/img/heroes/Dancz-icon.png" alt="dance icon"></span></button>
                    </div>
                    <div class="hero-tab-wrapper">
                        <button class="tablinks hero-tab" onclick="openTab(event, 'Muzo')">музо <span class="hero-icon-wrapper"><img class="hero-icon" src="<?php echo get_template_directory_uri() ?>/img/heroes/Muzo-icon.png" alt="muzo icon"></span></button>
                    </div>
                    <div class="hero-tab-wrapper">
                        <button class="tablinks hero-tab last-hero-tab" onclick="openTab(event, 'Tube')">туби <span class="hero-icon-wrapper"><img class="hero-icon" src="<?php echo get_template_directory_uri() ?>/img/heroes/Tubi-icon.png" alt="tube icon"></span></button>
                    </div>
              </div>
        </div>
        <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
            <div id="Myka" class="tabcontent hero-tab-content">
                <div class="hero-title-wrapper">
                    <h2 id="ya1" class="hero-title">МУка</h2>
                </div>
                <div  class="hero-img-wrapper">
                    <img src="<?php echo get_template_directory_uri() ?>/img/heroes/Myka.png" alt="Myka">
                </div>
                <div class="hero-text-wrapper">
                    <p class="hero-description">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_hero']); ?>
                    </p>
                    <p class="hero-description hero-description-star"><?php echo esc_attr($iq_gradus_options['iq_gradus_hero_1']); ?></p>
                </div>
              </div>
              <!-- /.tab Myka -->
              <div id="Semchy" class="tabcontent hero-tab-content">
                <div class="hero-title-wrapper">
                    <h2 id="ya2"  class="hero-title">семчу*</h2>
                </div>
                <div class="hero-img-wrapper">
                    <img src="<?php echo get_template_directory_uri() ?>/img/heroes/semchy.png" alt="Cemchy">
                </div>
                <div class="hero-text-wrapper">
                    <p class="hero-description">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_hero_2']); ?>
                    </p>
                    <p class="hero-description hero-description-star"><?php echo esc_attr($iq_gradus_options['iq_gradus_hero_3']); ?></p>
                </div>
              </div>
              <div id="Eg" class="tabcontent hero-tab-content">
                <div class="hero-title-wrapper">
                    <h2 id="ya3"  class="hero-title">ЕГЭн</h2>
                </div>
                <div class="hero-img-wrapper">
                    <img src="<?php echo get_template_directory_uri() ?>/img/heroes/eg.png" alt="eg">
                </div>
                <div class="hero-text-wrapper">
                    <p class="hero-description">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_hero_4']); ?>
                    </p>
                </div>
              </div>
              <div id="Cap" class="tabcontent hero-tab-content">
                <div class="hero-title-wrapper">
                    <h2 id="ya4"  class="hero-title">КАПИ</h2>
                </div>
                <div class="hero-img-wrapper">
                    <img src="<?php echo get_template_directory_uri() ?>/img/heroes/Cap.png" alt="cap">
                </div>
                <div class="hero-text-wrapper">
                    <p class="hero-description">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_hero_5']); ?>
                    </p>
                </div>
              </div>
              <div id="Dance" class="tabcontent hero-tab-content">
                <div class="hero-title-wrapper">
                    <h2 id="ya5"  class="hero-title">ДАнц</h2>
                </div>
                <div class="hero-img-wrapper">
                    <img src="<?php echo get_template_directory_uri() ?>/img/heroes/Dance.png" alt="dance">
                </div>
                <div class="hero-text-wrapper">
                    <p class="hero-description">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_hero_6']); ?>
                    </p>
                </div>
              </div>
              <div id="Muzo" class="tabcontent hero-tab-content">
                <div class="hero-title-wrapper">
                    <h2 id="ya6"  class="hero-title">музо*</h2>
                </div>
                <div class="hero-img-wrapper">
                    <img src="<?php echo get_template_directory_uri() ?>/img/heroes/Muzo.png" alt="Muzo">
                </div>
                <div class="hero-text-wrapper">
                    <p class="hero-description">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_hero_7']); ?>
                    </p>
                    <p class="hero-description hero-description-star"><?php echo esc_attr($iq_gradus_options['iq_gradus_hero_8']); ?></p>
                </div>
              </div>
              <div id="Tube" class="tabcontent hero-tab-content">
                <div class="hero-title-wrapper">
                    <h2 id="ya7"  class="hero-title">туби</h2>
                </div>
                <div class="hero-img-wrapper">
                    <img src="<?php echo get_template_directory_uri() ?>/img/heroes/too-be.png" alt="Tobee">
                </div>
                <div class="hero-text-wrapper">
                    <p class="hero-description">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_hero_9']); ?>
                    </p>
                </div>
              </div>
        </div>
    </div>
</div>
<!-- Modal video start-->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
                <div class="video-wrapper">
                <video width="100%" height="100%" controls src="<?php echo get_template_directory_uri() ?>/img/video/gradus2.mp4">
                </video>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--Modal video end-->
</section>

<?php get_template_part('template-parts/subscribe-capitan'); ?>

<?php get_footer(); ?>