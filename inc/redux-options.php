<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "iq_gradus_options";

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();
    
    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Настройки сайта', 'redux-framework-demo' ),
        'page_title'           => __( 'Настройки сайта', 'redux-framework-demo' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => false,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'komp_orel-docs',
        'href'  => '#',
        'title' => __( 'Documentation', 'komp_orel' ),
    );

    $args['admin_bar_links'][] = array(
        'href'  => '#',
        'title' => __( 'Support', 'komp_orel' ),
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.

    $args['share_icons'][] = array(
        'url'   => 'https://vk.com/neoart.studio',
        'title' => 'Группа разработчиков сайта в ВК',
        'icon'  => 'el el-vkontakte'
    );
   
    // Add content after the form.
    $args['footer_text'] = __( '<p></p>', 'redux-framework-demo' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START Basic Fields
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Настройки главной страницы', 'iq_gradus' ),
        'id'               => 'main_page',
        'desc'             => __( 'Настройка банера и слайдера', 'iq_gradus' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-home-alt'
    ) );

    Redux::setSection( $opt_name, array(
    'title'            => __( 'Банер', 'iq_gradus' ),
    'desc'             => __( '', 'iq_gradus' ),
    'id'               => 'site_baner',
    'subsection'       => true,
    'customizer_width' => '400px',
    'fields'           => array(
        array(
            'id'       => 'iq_gradus_baner_title',
            'type'     => 'text',
            'title'    => __( 'Заголовок банера', 'iq_gradus' ),
            'subtitle' => __( 'Введите заголовок для банера', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_baner_text',
            'type'     => 'textarea',
            'rows' => '3',
            'title'    => __( 'Текст банера', 'iq_gradus' ),
            'subtitle' => __( 'Введите текст банера', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_baner_link',
            'type'     => 'text',
            'title'    => __( 'Ссылка на страницу с играми', 'iq_gradus' ),
            'subtitle' => __( 'Вставьте ссылку на страницу с играми', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
    )
) );

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Настройки контактов и логотипа', 'iq_gradus' ),
        'id'               => 'basic',
        'desc'             => __( 'Настройка контактных данных, логотипа сайта, адреса и т.д.', 'iq_gradus' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-phone-alt'
    ) );

    Redux::setSection( $opt_name, array(
    'title'            => __( 'Логотип', 'iq_gradus' ),
    'desc'             => __( '', 'iq_gradus' ),
    'id'               => 'site_logos',
    'subsection'       => true,
    'customizer_width' => '400px',
    'fields'           => array(
        array(
            'id'       => 'iq_gradus_logo',
            'type'     => 'media',
            'url'       => true,
            'title'    => __( 'Лого', 'iq_gradus' ),
            'subtitle' => __( 'Загрузите ваш логотип', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Контакты', 'iq_gradus' ),
    'desc'             => __( 'Введите ваши контактные данные', 'iq_gradus' ),
    'id'               => 'header_data',
    'subsection'       => true,
    'customizer_width' => '400px',
    'fields'           => array(
        array(
            'id'       => 'iq_gradus_phone',
            'type'     => 'text',
            'title'    => __( 'Телефон', 'iq_gradus' ),
            'subtitle' => __( 'Введите ваш контактный телефон', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '+7 (910) 748-69-88',
        ),
        array(
            'id'       => 'iq_gradus_phone_link',
            'type'     => 'text',
            'title'    => __( 'Телефон для ссылки', 'iq_gradus' ),
            'subtitle' => __( 'Введите ваш контактный телефон без пробелов и лишних знаков', 'iq_gradus' ),
            'desc'     => __( 'Для ссылки нужно ввести телефон в формате +7 и далее номер без пробелов, тире и скобок', 'iq_gradus' ),
            'default'  => '+79107486988',
        ),
        array(
            'id'       => 'iq_gradus_email',
            'type'     => 'text',
            'validate' => 'email',
            'msg'      => __( 'Не верный email', 'iq_gradus' ),
            'title'    => __( 'Электронная почта', 'iq_gradus' ),
            'subtitle' => __( 'Введите ваш email', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_adress',
            'type'     => 'text',
            'title'    => __( 'Адресс', 'iq_gradus' ),
            'subtitle' => __( 'Введите адрес вашего офиса', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_politics',
            'type'     => 'text',
            'title'    => __( 'Ссылка на страницу пользовательсокого соглашения', 'iq_gradus' ),
            'subtitle' => __( 'Вставьте ссылку на страницу пользовательсокого соглашения', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_vk',
            'type'     => 'text',
            'title'    => __( 'Ссылка на группу ВК', 'iq_gradus' ),
            'subtitle' => __( 'Вставьте ссылку на группу ВК', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_inst',
            'type'     => 'text',
            'title'    => __( 'Ссылка на аккаунт в Instagram', 'iq_gradus' ),
            'subtitle' => __( 'Вставьте ссылку на аккаунт в Instagram', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_fav',
            'type'     => 'text',
            'title'    => __( 'Ссылка на страницу избранного', 'iq_gradus' ),
            'subtitle' => __( 'Вставьте ссылку на страницу избранного', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Настройки страницы о нас', 'iq_gradus' ),
    'id'               => 'about',
    'desc'             => __( 'Настройка страницы о нас, описание что такое интелектуальный градус и т.д', 'iq_gradus' ),
    'customizer_width' => '400px',
    'icon'             => 'el el-thumbs-up'
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Блок о нас', 'iq_gradus' ),
    'desc'             => __( '', 'iq_gradus' ),
    'id'               => 'block_about',
    'subsection'       => true,
    'customizer_width' => '400px',
    'fields'           => array(
        array(
            'id'       => 'iq_gradus_about_title',
            'type'     => 'text',
            'title'    => __( 'Заголовок - что такое интелектуальный градус', 'iq_gradus' ),
            'subtitle' => __( 'Введите ваш заголовок', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_about_text',
            'type'     => 'textarea',
            'rows' => '4',
            'title'    => __( 'Описание', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_about_link',
            'type'     => 'text',
            'title'    => __( 'Ссылка на страницу с играми', 'iq_gradus' ),
            'subtitle' => __( 'Вставьте ссылку на страницу с играми', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Блок преимуществ', 'iq_gradus' ),
    'desc'             => __( '', 'iq_gradus' ),
    'id'               => 'block_numbers',
    'subsection'       => true,
    'customizer_width' => '400px',
    'fields'           => array(
        array(
            'id'       => 'iq_gradus_number_1',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Первое преимущество', 'iq_gradus' ),
            'subtitle' => __( 'Введите краткое описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_number_2',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Второе преимущество', 'iq_gradus' ),
            'subtitle' => __( 'Введите краткое описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_number_3',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Третье преимущество', 'iq_gradus' ),
            'subtitle' => __( 'Введите краткое описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_number_4',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Четвертое преимущество', 'iq_gradus' ),
            'subtitle' => __( 'Введите краткое описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_number_5',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Пятое преимущество', 'iq_gradus' ),
            'subtitle' => __( 'Введите краткое описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_number_6',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Шестое преимущество', 'iq_gradus' ),
            'subtitle' => __( 'Введите краткое описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Блок faq', 'iq_gradus' ),
    'desc'             => __( '', 'iq_gradus' ),
    'id'               => 'block_faq',
    'subsection'       => true,
    'customizer_width' => '400px',
    'fields'           => array(
        array(
            'id'       => 'iq_gradus_faq_page-link',
            'type'     => 'text',
            'title'    => __( 'Ссылка на страницу FAQ', 'iq_gradus' ),
            'subtitle' => __( 'Вставьте ссылку на страницу FAQ', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_instructions_link',
            'type'     => 'text',
            'title'    => __( 'Ссылка на страницу с руководством к игре', 'iq_gradus' ),
            'subtitle' => __( 'Вставьте ссылку на страницу с руководством к игре', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Руководство к игре', 'iq_gradus' ),
    'id'               => 'instructions',
    'desc'             => __( 'Настройки для страницы руководство к игре', 'iq_gradus' ),
    'customizer_width' => '400px',
    'icon'             => 'el el-book'
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Особенности игры', 'iq_gradus' ),
    'desc'             => __( '', 'iq_gradus' ),
    'id'               => 'game_features',
    'subsection'       => true,
    'customizer_width' => '400px',
    'fields'           => array(
        array(
            'id'       => 'iq_gradus_game_features_1',
            'type'     => 'textarea',
            'rows' => '3',
            'title'    => __( 'Текстовый блок', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_features_2',
            'type'     => 'textarea',
            'rows' => '3',
            'title'    => __( 'Текстовый блок', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_features_3',
            'type'     => 'textarea',
            'rows' => '3',
            'title'    => __( 'Текстовый блок', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_features_4',
            'type'     => 'textarea',
            'rows' => '3',
            'title'    => __( 'Текстовый блок', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Ход игры', 'iq_gradus' ),
    'desc'             => __( '', 'iq_gradus' ),
    'id'               => 'game_play',
    'subsection'       => true,
    'customizer_width' => '400px',
    'fields'           => array(
        array(
            'id'       => 'iq_gradus_game_play_description',
            'type'     => 'textarea',
            'rows' => '5',
            'title'    => __( 'Описание хода игры', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_play_description_title_1',
            'type'     => 'text',
            'title'    => __( 'Заголовок - шаг первый', 'iq_gradus' ),
            'subtitle' => __( 'Введите заголовок', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_play_description_1',
            'type'     => 'textarea',
            'rows' => '4',
            'title'    => __( 'Описание - шаг первый', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_play_description_title_2',
            'type'     => 'text',
            'title'    => __( 'Заголовок - шаг второй', 'iq_gradus' ),
            'subtitle' => __( 'Введите заголовок', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_play_description_2',
            'type'     => 'textarea',
            'rows' => '4',
            'title'    => __( 'Описание - шаг второй', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_play_description_title_3',
            'type'     => 'text',
            'title'    => __( 'Заголовок - шаг третий', 'iq_gradus' ),
            'subtitle' => __( 'Введите заголовок', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_play_description_3',
            'type'     => 'textarea',
            'rows' => '4',
            'title'    => __( 'Описание - шаг третий', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_play_description_title_4',
            'type'     => 'text',
            'title'    => __( 'Заголовок - шаг четвертый', 'iq_gradus' ),
            'subtitle' => __( 'Введите заголовок', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_play_description_4',
            'type'     => 'textarea',
            'rows' => '4',
            'title'    => __( 'Описание - шаг четвертый', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_play_link',
            'type'     => 'text',
            'title'    => __( 'Ссылка на страницу с играми', 'iq_gradus' ),
            'subtitle' => __( 'Вставьте ссылку на страницу с играми', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Правила игры', 'iq_gradus' ),
    'desc'             => __( '', 'iq_gradus' ),
    'id'               => 'game_rules',
    'subsection'       => true,
    'customizer_width' => '400px',
    'fields'           => array(
        array(
            'id'       => 'iq_gradus_game_rules_title_1',
            'type'     => 'text',
            'title'    => __( 'Заголовок - правила 1-3 туров', 'iq_gradus' ),
            'subtitle' => __( 'Введите заголовок', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_rules_description_1',
            'type'     => 'editor',
            'title'    => __( 'Описание - 1-3 туров - первый текстовый блок', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание ', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
            'args'   => array(
                'wpautop' => true,
                'media_buttons' => false,
                'teeny'  => true,
                'textarea_rows'    => 5
            )
        ),
        array(
            'id'       => 'iq_gradus_game_rules_description_1_1',
            'type'     => 'editor',
            'title'    => __( 'Описание - 1-3 туров - второй текстовый блок', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание ', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
            'args'   => array(
                'wpautop' => true,
                'media_buttons' => false,
                'teeny'  => true,
                'textarea_rows'    => 5
            )
        ),
        array(
            'id'       => 'iq_gradus_game_rules_title_2',
            'type'     => 'text',
            'title'    => __( 'Заголовок - правила 4 тура', 'iq_gradus' ),
            'subtitle' => __( 'Введите заголовок', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_rules_description_2',
            'type'     => 'editor',
            'title'    => __( 'Описание - 4 тура - первый текстовый блок', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
            'args'   => array(
                'wpautop' => true,
                'media_buttons' => false,
                'teeny' => true,
                'textarea_rows'    => 5
            )
        ),
        array(
            'id'       => 'iq_gradus_game_rules_description_2_2',
            'type'     => 'editor',
            'title'    => __( 'Описание - 4 тура - второй текстовый блок', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
            'args'   => array(
                'wpautop' => true,
                'media_buttons' => false,
                'teeny' => true,
                'textarea_rows'    => 5
            )
        ),
        array(
            'id'       => 'iq_gradus_game_rules_title_3',
            'type'     => 'text',
            'title'    => __( 'Заголовок - победа в игре', 'iq_gradus' ),
            'subtitle' => __( 'Введите заголовок', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_game_rules_description_3',
            'type'     => 'editor',
            'title'    => __( 'Описание - победа в игре - первый текстовый блок', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
            'args'   => array(
                'wpautop' => true,
                'media_buttons' => false,
                'teeny'  => true,
                'textarea_rows'    => 5
            )
        ),
        array(
            'id'       => 'iq_gradus_game_rules_description_3_3',
            'type'     => 'editor',
            'title'    => __( 'Описание - победа в игре - второй текстовый блок', 'iq_gradus' ),
            'subtitle' => __( 'Введите описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
            'args'   => array(
                'wpautop' => true,
                'media_buttons' => false,
                'teeny' => true,
                'textarea_rows'    => 5
            )
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Настройки страницы персонажей', 'iq_gradus' ),
    'id'               => 'heroes_page',
    'desc'             => __( 'Описание персонажей', 'iq_gradus' ),
    'customizer_width' => '400px',
    'icon'             => 'el el-group-alt'
) );

Redux::setSection( $opt_name, array(
'title'            => __( 'Персонажи', 'iq_gradus' ),
'desc'             => __( '', 'iq_gradus' ),
'id'               => 'heroes',
'subsection'       => true,
'customizer_width' => '400px',
'fields'           => array(
    array(
        'id'       => 'iq_gradus_hero',
        'type'     => 'textarea',
        'rows' => '3',
        'title'    => __( 'Мука', 'iq_gradus' ),
        'subtitle' => __( 'Введите описание персонажа', 'iq_gradus' ),
        'desc'     => __( '', 'iq_gradus' ),
        'default'  => '',
    ),
    array(
        'id'       => 'iq_gradus_hero_1',
        'type'     => 'textarea',
        'rows' => '1',
        'title'    => __( 'Звездочка для Муки', 'iq_gradus' ),
        'subtitle' => __( 'Введите текст для звёздочки', 'iq_gradus' ),
        'desc'     => __( '', 'iq_gradus' ),
        'default'  => '',
    ),
    array(
        'id'       => 'iq_gradus_hero_2',
        'type'     => 'textarea',
        'rows' => '3',
        'title'    => __( 'Семчу', 'iq_gradus' ),
        'subtitle' => __( 'Введите описание персонажа', 'iq_gradus' ),
        'desc'     => __( '', 'iq_gradus' ),
        'default'  => '',
    ),
    array(
        'id'       => 'iq_gradus_hero_3',
        'type'     => 'textarea',
        'rows' => '1',
        'title'    => __( 'Звездочка для Семчу', 'iq_gradus' ),
        'subtitle' => __( 'Введите текст для звёздочки', 'iq_gradus' ),
        'desc'     => __( '', 'iq_gradus' ),
        'default'  => '',
    ),
    array(
        'id'       => 'iq_gradus_hero_4',
        'type'     => 'textarea',
        'rows' => '3',
        'title'    => __( 'ЕГЭн', 'iq_gradus' ),
        'subtitle' => __( 'Введите описание персонажа', 'iq_gradus' ),
        'desc'     => __( '', 'iq_gradus' ),
        'default'  => '',
    ),
    array(
        'id'       => 'iq_gradus_hero_5',
        'type'     => 'textarea',
        'rows' => '3',
        'title'    => __( 'КАПИ', 'iq_gradus' ),
        'subtitle' => __( 'Введите описание персонажа', 'iq_gradus' ),
        'desc'     => __( '', 'iq_gradus' ),
        'default'  => '',
    ),
    array(
        'id'       => 'iq_gradus_hero_6',
        'type'     => 'textarea',
        'rows' => '3',
        'title'    => __( 'ДАнц', 'iq_gradus' ),
        'subtitle' => __( 'Введите описание персонажа', 'iq_gradus' ),
        'desc'     => __( '', 'iq_gradus' ),
        'default'  => '',
    ),
    array(
        'id'       => 'iq_gradus_hero_7',
        'type'     => 'textarea',
        'rows' => '3',
        'title'    => __( 'Музо', 'iq_gradus' ),
        'subtitle' => __( 'Введите описание персонажа', 'iq_gradus' ),
        'desc'     => __( '', 'iq_gradus' ),
        'default'  => '',
    ),
    array(
        'id'       => 'iq_gradus_hero_8',
        'type'     => 'textarea',
        'rows' => '1',
        'title'    => __( 'Звездочка для Музо', 'iq_gradus' ),
        'subtitle' => __( 'Введите текст для звёздочки', 'iq_gradus' ),
        'desc'     => __( '', 'iq_gradus' ),
        'default'  => '',
    ),
    array(
        'id'       => 'iq_gradus_hero_9',
        'type'     => 'textarea',
        'rows' => '3',
        'title'    => __( 'Туби', 'iq_gradus' ),
        'subtitle' => __( 'Введите описание персонажа', 'iq_gradus' ),
        'desc'     => __( '', 'iq_gradus' ),
        'default'  => '',
    ),
)
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Настройки каталога игр', 'iq_gradus' ),
    'id'               => 'catalog_page',
    'desc'             => __( 'Настройки каталога', 'iq_gradus' ),
    'customizer_width' => '400px',
    'icon'             => 'el el-shopping-cart'
) );
Redux::setSection( $opt_name, array(
    'title'            => __( 'Каталог', 'iq_gradus' ),
    'desc'             => __( '', 'iq_gradus' ),
    'id'               => 'site_catalog_page',
    'subsection'       => true,
    'customizer_width' => '400px',
    'fields'           => array(
        array(
            'id'       => 'iq_gradus_catalog_title',
            'type'     => 'text',
            'title'    => __( 'Заголовок', 'iq_gradus' ),
            'subtitle' => __( 'Введите заголовок', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_catalog_text',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Текст для краткого описания в каталоге', 'iq_gradus' ),
            'subtitle' => __( 'Введите краткое описание', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Настройки страницы избранное', 'iq_gradus' ),
    'id'               => 'wishlist_page',
    'desc'             => __( 'Настройки каталога', 'iq_gradus' ),
    'customizer_width' => '400px',
    'icon'             => 'el el-heart-alt'
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Избранное', 'iq_gradus' ),
    'desc'             => __( '', 'iq_gradus' ),
    'id'               => 'site_wishlist_page',
    'subsection'       => true,
    'customizer_width' => '400px',
    'fields'           => array(
        array(
            'id'       => 'iq_gradus_my_games_link',
            'type'     => 'text',
            'title'    => __( 'Ссылка на страницу с купленными играми', 'iq_gradus' ),
            'subtitle' => __( 'Вставьте ссылку', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_wishlist_link',
            'type'     => 'text',
            'title'    => __( 'Ссылка на страницу избранного', 'iq_gradus' ),
            'subtitle' => __( 'Вставьте ссылку', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_edit_account_link',
            'type'     => 'text',
            'title'    => __( 'Ссылка на страницу редактирования аккаунта', 'iq_gradus' ),
            'subtitle' => __( 'Вставьте ссылку', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Настройки страницы FAQ', 'iq_gradus' ),
    'id'               => 'faq_page',
    'desc'             => __( 'Настройки FAQ', 'iq_gradus' ),
    'customizer_width' => '400px',
    'icon'             => 'el el-question-sign'
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Настройки для FAQ ', 'iq_gradus' ),
    'desc'             => __( '', 'iq_gradus' ),
    'id'               => 'site_faq_page',
    'subsection'       => true,
    'customizer_width' => '400px',
    'fields'           => array(
        array(
            'id'       => 'iq_gradus_faq_title_1',
            'type'     => 'text',
            'title'    => __( 'Заголовок', 'iq_gradus' ),
            'subtitle' => __( 'Введите текст вопроса', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_text_1',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Ответ на вопрос', 'iq_gradus' ),
            'subtitle' => __( 'Введите ответ на вопрос', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_title_2',
            'type'     => 'text',
            'title'    => __( 'Заголовок', 'iq_gradus' ),
            'subtitle' => __( 'Введите текст вопроса', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_text_2',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Ответ на вопрос', 'iq_gradus' ),
            'subtitle' => __( 'Введите ответ на вопрос', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_title_3',
            'type'     => 'text',
            'title'    => __( 'Заголовок', 'iq_gradus' ),
            'subtitle' => __( 'Введите текст вопроса', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_text_3',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Ответ на вопрос', 'iq_gradus' ),
            'subtitle' => __( 'Введите ответ на вопрос', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_title_4',
            'type'     => 'text',
            'title'    => __( 'Заголовок', 'iq_gradus' ),
            'subtitle' => __( 'Введите текст вопроса', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_text_4',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Ответ на вопрос', 'iq_gradus' ),
            'subtitle' => __( 'Введите ответ на вопрос', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_title_5',
            'type'     => 'text',
            'title'    => __( 'Заголовок', 'iq_gradus' ),
            'subtitle' => __( 'Введите текст вопроса', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_text_5',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Ответ на вопрос', 'iq_gradus' ),
            'subtitle' => __( 'Введите ответ на вопрос', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_title_6',
            'type'     => 'text',
            'title'    => __( 'Заголовок', 'iq_gradus' ),
            'subtitle' => __( 'Введите текст вопроса', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_text_6',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Ответ на вопрос', 'iq_gradus' ),
            'subtitle' => __( 'Введите ответ на вопрос', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_title_7',
            'type'     => 'text',
            'title'    => __( 'Заголовок', 'iq_gradus' ),
            'subtitle' => __( 'Введите текст вопроса', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_text_7',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Ответ на вопрос', 'iq_gradus' ),
            'subtitle' => __( 'Введите ответ на вопрос', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_title_8',
            'type'     => 'text',
            'title'    => __( 'Заголовок', 'iq_gradus' ),
            'subtitle' => __( 'Введите текст вопроса', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_text_8',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Ответ на вопрос', 'iq_gradus' ),
            'subtitle' => __( 'Введите ответ на вопрос', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_title_9',
            'type'     => 'text',
            'title'    => __( 'Заголовок', 'iq_gradus' ),
            'subtitle' => __( 'Введите текст вопроса', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_text_9',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Ответ на вопрос', 'iq_gradus' ),
            'subtitle' => __( 'Введите ответ на вопрос', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_title_10',
            'type'     => 'text',
            'title'    => __( 'Заголовок', 'iq_gradus' ),
            'subtitle' => __( 'Введите текст вопроса', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_text_10',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Ответ на вопрос', 'iq_gradus' ),
            'subtitle' => __( 'Введите ответ на вопрос', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_title_11',
            'type'     => 'text',
            'title'    => __( 'Заголовок', 'iq_gradus' ),
            'subtitle' => __( 'Введите текст вопроса', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_text_11',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Ответ на вопрос', 'iq_gradus' ),
            'subtitle' => __( 'Введите ответ на вопрос', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_title_12',
            'type'     => 'text',
            'title'    => __( 'Заголовок', 'iq_gradus' ),
            'subtitle' => __( 'Введите текст вопроса', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
        array(
            'id'       => 'iq_gradus_faq_text_12',
            'type'     => 'textarea',
            'rows' => '2',
            'title'    => __( 'Ответ на вопрос', 'iq_gradus' ),
            'subtitle' => __( 'Введите ответ на вопрос', 'iq_gradus' ),
            'desc'     => __( '', 'iq_gradus' ),
            'default'  => '',
        ),
    )
) );






    /*
     *
     * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
     *
     */

    /*
    *
    * --> Action hook examples
    *
    */

    // If Redux is running as a plugin, this will remove the demo notice and links
    //add_action( 'redux/loaded', 'remove_demo' );

    // Function to test the compiler hook and demo CSS output.
    // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
    //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

    // Change the arguments after they've been declared, but before the panel is created
    //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

    // Change the default value of a field after it's been set, but before it's been useds
    //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

    // Dynamically add a section. Can be also used to modify sections/fields
    //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $field['msg']    = 'your custom error message';
                $return['error'] = $field;
            }

            if ( $warning == true ) {
                $field['msg']      = 'your custom warning message';
                $return['warning'] = $field;
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => __( 'Section via hook', 'redux-framework-demo' ),
                'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    /**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }

