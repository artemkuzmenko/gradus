<?php

class Iq_Gradus_Filter_Widget extends WP_Widget
{

    /**
     * General Setup.
     */
    public function __construct() {

        /* Widget settings. */
        $widget_ops = array(
            'classname' => 'iq_gradus_filter_widget',
            'description' => 'Виджет который выводит блок Ajax Фильтрация'
        );
        /* Widget control settings. */
        $control_ops = array(
            'width'		=> 500,
            'height'	=> 450,
            'id_base'	=> 'iq_gradus_filter_widget'
        );
        /* Create the widget. */
        parent::__construct( 'iq_gradus_filter_widget', 'Ajax Фильтрация', $widget_ops, $control_ops );
    }

    /**
     * Display Widget
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance )
    {
        extract( $args );
        $title1 = $instance['title1'];
        $title2 = $instance['title2'];

        global  $woocommerce;

        // Display Widget
        ?>
    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
        <div class="categories side-nav side-nav-mobile">
            <div class="categories-title-wrapper">
            <h5 class="categories__title"><?php echo $title1; ?></h5>
            <a class="date-filter-modal-toggle filter-modal-toggle-icon" href="#" data-toggle="modal" data-target="#filterModal">
            <?php echo $title1; ?>
            </a>
            </div>
            <div class="filter-on-themes-wrapper">
            По темам:
            </div>
            <div id="st-accordion" class="st-accordion">

                <ul>
                    <?php

                    $categories = get_terms(
                        'product_cat',
                        array(
                            'orderby'    => 'name',
                            'hierarchical' => true,
                            'hide_empty' => 1,
                            'parent' => 0
                        )
                    );
                    foreach($categories as $cat){ ?>
                    <li class="iq_gradus_filter_check">
                        <?php $temp_cat = get_terms(
                            'product_cat',
                            array(
                                'orderby'    => 'name',
                                'hierarchical' => true,
                                'hide_empty' => 1
                            )
                        );
                        $class='';
                        if($temp_cat) {$class='has_child';} else {$class='no_child';} ?>
                        <label class="label-for-checbox">
                        <div class="checkbox-info-wrapper">
                        <span class="<?php echo $class; ?>"><?php echo $cat->name; ?></span>
                        <span class="cat-name-count"><?php echo $cat->count; ?></span>
                        </div>
                        <input class="filter-check-box" id="term_<?php echo $cat->term_id; ?>" type="checkbox" name="category" 
                        value="<?php echo $cat->term_id; ?>">
                        <span class="filter-check-box-fake"></span>
                        </label>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
        <div class="categories side-nav side-nav-mobile">
            <div class="categories-title-wrapper">
            <h5 class="categories__title date-filter-title"><?php echo $title2; ?></h5>
            <a class="date-filter-modal-toggle date-filter-modal-toggle-icon" href="#"  data-toggle="modal" data-target="#dateModal">
            <?php echo $title2; ?>
            </a>
            </div>
            <div class="filter-on-themes-wrapper">
            По времени:
            </div>
            <div id="st-accordion" class="st-accordion">
            <div class="iq_gradus_filter_check">
            <label class="label-for-radio">
            <span>От новых к старым</span> <input class="date-radio-filter" type="radio" name="date" value="ASC" />
            <span class="date-radio-filter-fake"></span> 
            </label>
            </div>
            <div class="iq_gradus_filter_check">
            <label class="label-for-radio">
            <span>От старых к новым</span> <input class="date-radio-filter" type="radio" name="date" value="DESC" />
            <span class="date-radio-filter-fake"></span>
            </label>
            </div>
            </div>
        </div>
    </div>

<!-- Modal date -->
 <div class="modal fade" id="dateModal" tabindex="-1" role="dialog" aria-labelledby="dateModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <div class="categories-title-wrapper">
            <h5 class="date-filter-modal-toggle"><?php echo $title2; ?></h5>
            </div>
            </div>
      <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
        <div class="categories side-nav log">
            <div class="filter-on-themes-wrapper">
            По времени:
            </div>
            <div id="st-accordion" class="st-accordion">
            <div class="iq_gradus_filter_check">
            <label class="label-for-radio">
            <span>От новых к старым</span> <input class="date-radio-filter" type="radio" name="date" value="ASC" />
            <span class="date-radio-filter-fake"></span> 
            </label>
            </div>
            <div class="iq_gradus_filter_check">
            <label class="label-for-radio">
            <span>От старых к новым</span> <input class="date-radio-filter" type="radio" name="date" value="DESC" />
            <span class="date-radio-filter-fake"></span>
            </label>
            </div>
            </div>
        </div>
    </div>  
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="filter-buttons-wrapper">
            <input class="modal-button-reload" type='button' onclick='window.location.reload()'  value ='Сбросить' />
            <button type="button" class="outline-button" data-dismiss="modal">Применить</button>
            </div>     
            </div>  
      </div>
    </div>
  </div>
</div>
<!-- Modal filter -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <div class="categories-title-wrapper">
            <h5 class="date-filter-modal-toggle"><?php echo $title1; ?></h5>
            </div>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
        <div class="categories side-nav">
            <div class="filter-on-themes-wrapper">
            По темам:
            </div>
            <div id="st-accordion" class="st-accordion">

                <ul>
                    <?php

                    $categories = get_terms(
                        'product_cat',
                        array(
                            'orderby'    => 'name',
                            'hierarchical' => true,
                            'hide_empty' => 1,
                            'parent' => 0
                        )
                    );
                    foreach($categories as $cat){ ?>
                    <li class="iq_gradus_filter_check">
                        <?php $temp_cat = get_terms(
                            'product_cat',
                            array(
                                'orderby'    => 'name',
                                'hierarchical' => true,
                                'hide_empty' => 1
                            )
                        );
                        $class='';
                        if($temp_cat) {$class='has_child';} else {$class='no_child';} ?>
                        <label class="label-for-checbox">
                        <div class="checkbox-info-wrapper">
                        <span class="<?php echo $class; ?>"><?php echo $cat->name; ?></span>
                        <span class="cat-name-count"><?php echo $cat->count; ?></span>
                        </div>
                        <input class="filter-check-box" id="term_<?php echo $cat->term_id; ?>" type="checkbox" name="category" 
                        value="<?php echo $cat->term_id; ?>">
                        <span class="filter-check-box-fake"></span>
                        </label>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="filter-buttons-wrapper">
            <input class="modal-button-reload" type='button' onclick='window.location.reload()'  value ='Сбросить' />
            <button type="button" class="outline-button" data-dismiss="modal">Применить</button>
            </div>     
            </div>  
      </div>
    </div>
  </div>
</div>  
<?php

    }

    /**
     * Update Widget
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update( $new_instance, $old_instance )
    {
        $instance = $old_instance;
        $instance['title1'] = strip_tags( $new_instance['title1'] );
        $instance['title2'] = strip_tags( $new_instance['title2'] );
        return $instance;
    }

    /**
     * Widget Settings
     * @param array $instance
     */
    public function form( $instance )
    {
        //default widget settings.
        $defaults = array(
            'title1'	=> 'Фильтры',
            'title2'		=> 'Сортировка игр',
        );
        $instance = wp_parse_args( (array) $instance, $defaults );

        ?>
<p>
    <label for="<?php echo $this->get_field_id( 'title1' ); ?>">Фильтрация по категории | Заголовок</label>
    <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title1' ); ?>"
        name="<?php echo $this->get_field_name( 'title1' ); ?>" value="<?php echo $instance['title1']; ?>" />
</p>
<p>
            <label for="<?php echo $this->get_field_id( 'title2' ); ?>">Фильтрация по дате | Заголовок</label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title2' ); ?>" 
            name="<?php echo $this->get_field_name( 'title2' ); ?>" value="<?php echo $instance['title2']; ?>" />
        </p>

<?php
    }
}