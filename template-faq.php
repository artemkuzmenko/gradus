<?php
/**
 * Template name: FAQ
 */

get_header();
?>

<?php get_template_part('template-parts/page-title'); ?>

<section class="faq">
  <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
    <div class="row">
      <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
        <div class="tab tab-faq">
          <button class="tablinks" onclick="openTab(event, 'Comon')" id="defaultOpen">Особенности игры</button>
          <button class="tablinks" onclick="openTab(event, 'Teh')">Технические вопросы</button>
          <button class="tablinks" onclick="openTab(event, 'Game-question')">Вопросы к играм</button>
          <button class="tablinks" onclick="openTab(event, 'Mistakes')">Частые ошибки</button>
        </div>
      </div>
      <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
        <div id="Comon" class="tabcontent">
          <div id="accordion">
            <div class="faq-wrapper">
              <div class="faq-btn-wrapper" id="heading1">
                <h5 class="mb-0">
                  <button class="faq-btn collapsed" data-toggle="collapse" data-target="#collapse1" aria-expanded="true"
                    aria-controls="collapse1">
                    <span class="faq-btn-text-wrapper"><?php echo esc_attr($iq_gradus_options['iq_gradus_faq_title_1']); ?></span>
                  </button>
                </h5>
              </div>
              <div id="collapse1" class="collapse show" aria-labelledby="heading1" data-parent="#accordion">
                <div class="faq-body">
                <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_text_1']); ?>
                </div>
              </div>
            </div>
            <div class="faq-wrapper">
              <div class="faq-btn-wrapper" id="heading2">
                <h5 class="mb-0">
                  <button class="faq-btn" data-toggle="collapse" data-target="#collapse2"
                    aria-expanded="false" aria-controls="collapse2">
                    <span class="faq-btn-text-wrapper">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_title_2']); ?>
                    </span>
                  </button>
                </h5>
              </div>
              <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion">
                <div class="faq-body">
                <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_text_2']); ?>
                </div>
              </div>
            </div>
            <div class="faq-wrapper">
              <div class="faq-btn-wrapper" id="heading3">
                <h5 class="mb-0">
                  <button class="faq-btn collapsed" data-toggle="collapse" data-target="#collapse3"
                    aria-expanded="false" aria-controls="collapse3">
                    <span class="faq-btn-text-wrapper">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_title_3']); ?>
                    </span>
                  </button>
                </h5>
              </div>
              <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion">
                <div class="faq-body">
                <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_text_3']); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.tab comon -->
        <div id="Teh" class="tabcontent">
          <div id="accordion">
            <div class="faq-wrapper">
              <div class="faq-btn-wrapper" id="heading4">
                <h5 class="mb-0">
                  <button class="faq-btn" data-toggle="collapse" data-target="#collapse4" aria-expanded="true"
                    aria-controls="collapse4">
                    <span class="faq-btn-text-wrapper">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_title_4']); ?>
                    </span>
                  </button>
                </h5>
              </div>
              <div id="collapse4" class="collapse show" aria-labelledby="heading4" data-parent="#accordion">
                <div class="faq-body">
                <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_text_4']); ?>
                </div>
              </div>
            </div>
            <div class="faq-wrapper">
              <div class="faq-btn-wrapper" id="heading5">
                <h5 class="mb-0">
                  <button class="faq-btn collapsed" data-toggle="collapse" data-target="#collapse5"
                    aria-expanded="false" aria-controls="collapseFive">
                    <span class="faq-btn-text-wrapper">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_title_5']); ?>
                    </span>
                  </button>
                </h5>
              </div>
              <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
                <div class="faq-body">
                <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_text_5']); ?>
                </div>
              </div>
            </div>
            <div class="faq-wrapper">
              <div class="faq-btn-wrapper" id="heading6">
                <h5 class="mb-0">
                  <button class="faq-btn collapsed" data-toggle="collapse" data-target="#collapse6"
                    aria-expanded="false" aria-controls="collapse6">
                    <span class="faq-btn-text-wrapper">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_title_6']); ?>
                    </span>
                  </button>
                </h5>
              </div>
              <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
                <div class="faq-body">
                <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_text_6']); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.tab teh -->
        <div id="Game-question" class="tabcontent">
          <div id="accordion">
            <div class="faq-wrapper">
              <div class="faq-btn-wrapper" id="heading7">
                <h5 class="mb-0">
                  <button class="faq-btn" data-toggle="collapse" data-target="#collapse7" aria-expanded="true"
                    aria-controls="collapse7">
                    <span class="faq-btn-text-wrapper">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_title_7']); ?>
                    </span>
                  </button>
                </h5>
              </div>
              <div id="collapse7" class="collapse show" aria-labelledby="heading7" data-parent="#accordion">
                <div class="faq-body">
                <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_text_7']); ?>
                </div>
              </div>
            </div>
            <div class="faq-wrapper">
              <div class="faq-btn-wrapper" id="heading8">
                <h5 class="mb-0">
                  <button class="faq-btn collapsed" data-toggle="collapse" data-target="#collapse8"
                    aria-expanded="false" aria-controls="collapseTwo">
                    <span class="faq-btn-text-wrapper">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_title_8']); ?>
                    </span>
                  </button>
                </h5>
              </div>
              <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordion">
                <div class="faq-body">
                <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_text_8']); ?>
                </div>
              </div>
            </div>
            <div class="faq-wrapper">
              <div class="faq-btn-wrapper" id="heading9">
                <h5 class="mb-0">
                  <button class="faq-btn collapsed" data-toggle="collapse" data-target="#collapse9"
                    aria-expanded="false" aria-controls="collapse9">
                    <span class="faq-btn-text-wrapper">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_title_9']); ?>
                    </span>
                  </button>
                </h5>
              </div>
              <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordion">
                <div class="faq-body">
                <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_text_9']); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.tab game questions -->
        <div id="Mistakes" class="tabcontent">
          <div id="accordion">
            <div class="faq-wrapper">
              <div class="faq-btn-wrapper" id="heading10">
                <h5 class="mb-0">
                  <button class="faq-btn" data-toggle="collapse" data-target="#collapse10" aria-expanded="true"
                    aria-controls="collapse10">
                    <span class="faq-btn-text-wrapper">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_title_10']); ?>
                    </span>
                  </button>
                </h5>
              </div>
              <div id="collapse10" class="collapse show" aria-labelledby="heading10" data-parent="#accordion">
                <div class="faq-body">
                <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_text_10']); ?>
                </div>
              </div>
            </div>
            <div class="faq-wrapper">
              <div class="faq-btn-wrapper" id="heading11">
                <h5 class="mb-0">
                  <button class="faq-btn collapsed" data-toggle="collapse" data-target="#collapse11"
                    aria-expanded="false" aria-controls="collapse11">
                    <span class="faq-btn-text-wrapper">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_title_11']); ?>
                    </span>
                  </button>
                </h5>
              </div>
              <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordion">
                <div class="faq-body">
                <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_text_11']); ?>
                </div>
              </div>
            </div>
            <div class="faq-wrapper">
              <div class="faq-btn-wrapper" id="heading12">
                <h5 class="mb-0">
                  <button class="faq-btn collapsed" data-toggle="collapse" data-target="#collapse12"
                    aria-expanded="false" aria-controls="collapse12">
                    <span class="faq-btn-text-wrapper">
                    <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_title_12']); ?>
                    </span>
                  </button>
                </h5>
              </div>
              <div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordion">
                <div class="faq-body">
                <?php echo esc_attr($iq_gradus_options['iq_gradus_faq_text_12']); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.tab game questions -->
      </div>
      <!-- /.col-xl-3 end -->
    </div>
    <!-- /.row end -->
  </div>
  <!-- /.container end -->
</section>

<?php get_template_part('template-parts/subscribe-capitan'); ?>

<?php get_footer(); ?>