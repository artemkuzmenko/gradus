<?php
/**
 * Order Downloads.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-downloads.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="woocommerce-order-downloads">
	<div
		class="woocommerce-table woocommerce-table--order-downloads shop_table shop_table_responsive order_details row">
		<?php foreach ( $downloads as $download ) : ?>
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
			<div class="product-card product-card-account">
				<div class="product-card-info product-card-account-wrapper">
					<?php foreach ( wc_get_account_downloads_columns() as $column_id => $column_name ) : ?>
					<div class="<?php echo esc_attr( $column_id ); ?>"
						data-title="<?php echo esc_attr( $column_name ); ?>">
						<?php
						if ( has_action( 'woocommerce_account_downloads_column_' . $column_id ) ) {
							do_action( 'woocommerce_account_downloads_column_' . $column_id, $download );
						} else {
							switch ( $column_id ) {
								case 'download-product':
									if ( $download['product_url'] ) {
										echo '<h3 class="product-title">';
										echo '<a href="' . esc_url( $download['product_url'] ) . '">' . esc_html( $download['product_name'] ) . '</a>';
										echo '</h3>';
									} else {
										echo esc_html( $download['product_name'] );
									}
									break;
									case 'download-expires':
										if ( ! empty( $download['access_expires'] ) ) {
											echo '<div class="product-card-account-info">';
											echo '<div class="product-card-days"> Игра доступна до:';
											echo '<span class="number-of-days"><time datetime="' . esc_attr( date( 'y-m-d', strtotime( $download['access_expires'] ) ) ) . '" title="' . esc_attr( strtotime( $download['access_expires'] ) ) . '">' . esc_html( date_i18n( get_option( 'date_format' ), strtotime( $download['access_expires'] ) ) ) . '</time></span>';
											echo '</div>';
											echo '<div class="product-card-question-icon">';
											echo '<span class="tooltiptext">Количество игр не ограничено, игра доступна в течении трех дней с момента покупки</span>';
											echo '<div class="product-card-question-icon-bg"></div>';
											echo '</div>';
											echo '</div>';
										} else {
											echo '<div class="product-card-account-info">';
											echo '<div class="product-card-days">';
											echo '<span class="product-card-days-unlim">&#8734;</span>';
											echo '</div>';
											echo '<div class="product-card-question-icon">';
											echo '<span class="tooltiptext">Количество игр не ограничено, игра доступна вам навсегда</span>';
											echo '<div class="product-card-question-icon-bg"></div>';
											echo '</div>';
											echo '</div>';
										}
										break;
									case 'download-file':
									echo '<div class="product-card-button-wrapper">';
									echo '<a target="_blank" href="' . esc_url( $download['download_url'] ) . '" class="add-to-cart outline-button digital-download-btn">' . esc_html( $download['download_name'] ) . '</a>';
									echo '</div>';
									break;
							}
						}
						?>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
</div>