<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_cart' ); ?>
<?php global $iq_gradus_options; ?>
<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>

	<div class="shop_table shop_table_responsive cart woocommerce-cart-form__contents row" cellspacing="0">
		<div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
			<div
				class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
				<div class="product-basket-wrapper">
					<div class="product-thumbnail product-basket-image">
						<?php
						$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

						if ( ! $product_permalink ) {
							echo $thumbnail; // PHPCS: XSS ok.
						} else {
							printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
						}
						?>
					</div>
					<div class="product-basket-info">
						<div class="product-name product-basket-title"
							data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
							<?php
						if ( ! $product_permalink ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
						} else {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a class="product-link" href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
						}

						do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

						// Meta data.
						echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

						// Backorder notification.
						if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
						}
						?>
						</div>

						<div class="product-price product-basket-price"
							data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
							<?php
								echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
							?>
						</div>
					</div>
					<div class="product-remove cross-wrapper">
						<?php
								echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
									'woocommerce_cart_item_remove_link',
									sprintf(
										'<a href="%s" class="remove product-link" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
										esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
										esc_html__( 'Remove this item', 'woocommerce' ),
										esc_attr( $product_id ),
										esc_attr( $_product->get_sku() )
									),
									$cart_item_key
								);
							?>
					</div>
				</div>
			</div>
			<?php
				}
			}
			?>

			<?php do_action( 'woocommerce_cart_contents' ); ?>
		</div>
		<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-4 col-sm-12 col-12">
					<div class="actions coupon-wrapper">
						<div class="coupon-header">
							Купон
						</div>
						<?php if ( wc_coupons_enabled() ) { ?>
						<div class="coupon">
							<div class="basket-input-wrapper">
								<input type="text" name="coupon_code"
									class="input-text subscribe-form-input basket-form-input" id="coupon_code" value=""
									placeholder="Введите номер" />
							</div>
							<div class="coupon-button-wrapper">
								<button type="submit" class="button basket-coupon-button" name="apply_coupon"
									value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>">Использовать купон</button>
							</div>
							<?php do_action( 'woocommerce_cart_coupon' ); ?>
						</div>
						<?php } ?>
						<?php do_action( 'woocommerce_cart_actions' ); ?>
						<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
					</div>
				</div>
				<div class="col-xl-12 col-lg-12 col-md-8 col-sm-12 col-12">
					<div class="cart-collaterals order-info-wrapper">
						<?php
		/**
		 * Cart collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 */
		do_action( 'woocommerce_cart_collaterals' );
	?>
						
						<div class="order-basket-text-wrapper">
							Нажимая на кнопку "Оформить заказ", вы соглашаетесь с <a class="green-link link-hover"
								href="<?php echo esc_url($iq_gradus_options['iq_gradus_politics']); ?>" target="blank">пользовательским
								соглашением.</a>
						</div>
					
					</div>
				</div>
			</div>
		</div>
		<?php do_action( 'woocommerce_after_cart_contents' ); ?>
	</div>
	<!-- /.row end -->
	<?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>
<?php do_action( 'woocommerce_before_cart_collaterals' ); ?>

<?php do_action( 'woocommerce_after_cart' ); ?>