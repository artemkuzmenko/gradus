<?php 
if ( ! defined('ABSPATH') ) {
    exit; 
}

add_action( 'woocommerce_before_single_product', 'iq_gradus_wrapper_product_start', 5 );
function iq_gradus_wrapper_product_start() {
	?>
<section class="product-card-section">
	<div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
		<?php
}
add_action( 'woocommerce_after_single_product', 'iq_gradus_wrapper_product_end', 5 );
function iq_gradus_wrapper_product_end() {
	?>
	</div>
</section>
<?php get_template_part('template-parts/cross-sale'); ?>
<?php get_template_part('template-parts/subscribe-capitan'); ?>
<?php
}
add_action( 'woocommerce_before_single_product_summary', 'iq_gradus_wrapper_product_image_start', 5 );
function iq_gradus_wrapper_product_image_start() {
	?>
<div class="row">
	<div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
		<div class="product-card-image-wrapper">
			<?php
}
add_action( 'woocommerce_before_single_product_summary', 'iq_gradus_wrapper_product_image_end', 25 );
function iq_gradus_wrapper_product_image_end() {
	?>
		</div>
	</div>
	<?php
}
add_action( 'woocommerce_before_single_product_summary', 'iq_gradus_wrapper_product_entry_start', 30 );
function iq_gradus_wrapper_product_entry_start() {
	?>
	<div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
		<div class="product-card-description-and-share">
			<div class="description-title">
				Описание / Сюжет
			</div>
			<div class="share-wrapper">
				<span class="words-share">Поделиться</span>
				<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter,viber,whatsapp,telegram"
					data-limit="0"></div>
			</div>
			</div>
			<?php
}
add_action( 'woocommerce_after_single_product_summary', 'iq_gradus_wrapper_product_entry_end', 5 );
function iq_gradus_wrapper_product_entry_end() {
	?>
	</div>
</div>
<?php
}
//Product summary (title,button,price,etc)
add_filter('woocommerce_single_product_summary', 'iq_gradus_short_description', 10);
function iq_gradus_short_description() {
 ?>
<div class="single-product-card-description">
		<?php echo the_content(); ?>
    </div>
<?php
}
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10); 
function woocommerce_template_single_price() {
	?>
	   <div class="product-card-buy-block-wrapper">
                        <div class="product-card-price">
						<?php woocommerce_template_loop_price(); ?>
                        </div>
                        <div class="single-product-card-button-wrapper">
						<?php woocommerce_template_loop_add_to_cart(); ?>
                        <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id="'.get_the_ID().'" variation_id="0"]'); ?>
                        </div>
                    </div>
	<?php
}
add_action('woocommerce_single_product_summary', 'iq_gradus_template_single_add_to_cart', 15); 
  

//Product slider 
add_filter('woocommerce_single_product_carousel_options', 'ud_update_woo_flexslider_options');
function ud_update_woo_flexslider_options($options) {
      // show arrows
      $options['directionNav'] = true;
      $options['animation'] = "slide";

      // controls
      $options['controlNav'] = true;
	  $options['controlNav'] = "thumbnails";
	  
      return $options;
  }

  //Remove lightbox 
  add_filter('woocommerce_single_product_image_thumbnail_html','wc_remove_link_on_thumbnails' );
  function wc_remove_link_on_thumbnails( $html ) {
	   return strip_tags( $html,'<div><img>' );
  }