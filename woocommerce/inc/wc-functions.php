<?php 
if ( ! defined('ABSPATH') ) {
    exit; 
}

//Sidebar
add_action('woocommerce_before_main_content', 'iq_gradus_only_archive_sidebar', 50);
function iq_gradus_only_archive_sidebar() {
 if (!is_product()) {
    woocommerce_get_sidebar();
 }
}

//Breadcrumb
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20); 
// Shop
add_action('woocommerce_before_main_content', 'iq_gradus_breadcrumb', 20);
function iq_gradus_breadcrumb() {
  if(is_shop()){
    ?>
  <?php get_template_part('template-parts/woo-page-title'); ?>
<?php 
}
}
// Product
add_action('woocommerce_before_main_content', 'iq_gradus_breadcrumb_product', 20);
function iq_gradus_breadcrumb_product() {
  if(is_product()){
    ?>
  <?php get_template_part('template-parts/page-title'); ?>
<?php 
}
}

//Empty cart 
add_action('woocommerce_cart_is_empty', 'iq_gradus_empty_cart_message', 10);
function iq_gradus_empty_cart_message() {
  echo '<div class="cart-empty woocommerce-info">' . wp_kses_post( apply_filters( 'wc_empty_cart_message', __( 'Your cart is currently empty.', 'woocommerce' ) ) ) . '</div>';
  ?>
<?php 
}

//Account wrapper 
add_action( 'woocommerce_before_account_navigation', 'iq_gradus_account_wrapper_start' );
function iq_gradus_account_wrapper_start(){
  ?>
  <div class="row">
        <?php
}

add_action( 'woocommerce_account_content', 'iq_gradus_account_wrapper_end' );
function iq_gradus_account_wrapper_end(){
	?>
		</div>
	<?php
}




