<?php 
if ( ! defined('ABSPATH') ) {
    exit; 
}

add_action( 'woocommerce_before_main_content', 'iq_gradus_archive_wrapper_start', 40 );
function iq_gradus_archive_wrapper_start(){
if(is_shop() || is_product_taxonomy()){
?>
<div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
    <div class="row">
        <?php
}
}
add_action( 'woocommerce_after_main_content', 'iq_gradus_archive_wrapper_end', 30 );
function iq_gradus_archive_wrapper_end(){
if(is_shop() || is_product_taxonomy()){
?>
    </div>
</div>
<?php
}
}

add_action( 'woocommerce_before_main_content', 'iq_gradus_archive_content_wrapper_start', 60 );
function iq_gradus_archive_content_wrapper_start(){
    global $iq_gradus_options;
    if(is_shop() || is_product_taxonomy()){
	?>
<div id="filter-container" class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
<div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="category-description-title">
                            Описание
                        </div>
                        <div class="category-description">
                            <h3><?php echo esc_attr($iq_gradus_options['iq_gradus_catalog_title']); ?></h3>
                            <p><?php echo esc_attr($iq_gradus_options['iq_gradus_catalog_text']); ?></p>
                        </div>

                    </div>
                </div>
    <?php
}
}
add_action( 'woocommerce_after_main_content', 'iq_gradus_archive_content_wrapper_end', 25 );
function iq_gradus_archive_content_wrapper_end(){
if(is_shop() || is_product_taxonomy()){
	?>
</div>
<?php get_template_part('template-parts/subscribe-capitan'); ?>
<?php
}
}

