<?php 
if ( ! defined('ABSPATH') ) {
    exit; 
}

//Product cards
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);


add_action( 'woocommerce_before_shop_loop_item', 'iq_gradus_loop_product_div_open' , 5);
function iq_gradus_loop_product_div_open(){
	?>
	<div class="product-card shop-product-card">
	<div class="product-img">
                        <a href="<?php the_permalink(); ?>">
                        <?php if( get_field('woo_custom_image_in_slider') ): ?>
                        <img src="<?php the_field('woo_custom_image_in_slider'); ?>" />
                        <?php endif; ?>
                        </a>
                        </div>
	<?php
}

add_action( 'woocommerce_shop_loop_item_title', 'iq_gradus_template_loop_product_title', 10);
function iq_gradus_template_loop_product_title(){
	?>
	 <div class="product-card-info">
                            <h3 class="product-title"><a class="product-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <div class="product-card-description">
                            <a class="product-link" href="<?php the_permalink(); ?>">
                                <?php echo get_the_excerpt() ?>
                            </a>
                            </div>
                            <div class="product-price">
                            <?php woocommerce_template_loop_price(); ?>
                            </div>
                            <div class="basket-and-like-wrapper product-card-basket">
                            <?php woocommerce_template_loop_add_to_cart(); ?>
                            <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id="'.get_the_ID().'" variation_id="0"]'); ?>
                            </div>
                        </div>
	<?php
}

add_action( 'woocommerce_after_shop_loop_item', 'iq_gradus_loop_product_div_close' , 20);
function iq_gradus_loop_product_div_close(){
	?>
	</div>
	
	<?php
}












