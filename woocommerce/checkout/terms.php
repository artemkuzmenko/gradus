<?php
/**
 * Checkout terms and conditions area.
 *
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

if ( apply_filters( 'woocommerce_checkout_show_terms', true ) && function_exists( 'wc_terms_and_conditions_checkbox_enabled' ) ) {
	do_action( 'woocommerce_checkout_before_terms_and_conditions' );

	?>
	<?php global $iq_gradus_options; ?>
	<div class="woocommerce-terms-and-conditions-wrapper">
		<?php
		/**
		 * Terms and conditions hook used to inject content.
		 *
		 * @since 3.4.0.
		 * @hooked wc_checkout_privacy_policy_text() Shows custom privacy policy text. Priority 20.
		 * @hooked wc_terms_and_conditions_page_content() Shows t&c page content. Priority 30.
		 */
		do_action( 'woocommerce_checkout_terms_and_conditions' );
		?>

		<?php if ( wc_terms_and_conditions_checkbox_enabled() ) : ?>
			<div class="validate-required iq-gradus-validate-required">
				<div class="robokassa-logo-wrapper">
					<div class="robokassa-text">
					Система обслуживается:
					</div>
					<div class="robokassa-logo">
						<img src="<?php echo get_template_directory_uri() ?>/img/main/robokassa.svg" alt="robokassa logo">
					</div>
				</div>
				<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox"> 
					<span class="woocommerce-terms-and-conditions-checkbox-text">Нажимая на кнопку вы соглашаетесь с <a targer="blank" class="green-link link-hover" href="<?php echo esc_url($iq_gradus_options['iq_gradus_politics']); ?>">пользовательским соглашением</a></span>
				</label>
			</div>
		<?php endif; ?>
	</div>
	<?php

	do_action( 'woocommerce_checkout_after_terms_and_conditions' );
}
