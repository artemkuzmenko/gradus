<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_edit_account_form' ); ?>
<form class="woocommerce-EditAccountForm edit-account" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?> >
	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>
	<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
	<div class="account-input-wrapper">
		<input type="email" class="woocommerce-Input woocommerce-Input--email input-text subscribe-form-input account-form-input" placeholder="Ваш логин" name="account_email" id="account_email" autocomplete="email" value="<?php echo esc_attr( $user->user_email ); ?>" />
	</div>
	</div>
	<div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
		<div class="account-input-wrapper">
			<input id="password-input" type="password" class="woocommerce-Input woocommerce-Input--password input-text subscribe-form-input account-form-input" placeholder="Ваш пароль" name="password_current" id="password_current" autocomplete="off" />
		</div>
		</div>
		<div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
		<div class="account-input-wrapper">
			<input id="password-input" type="password" class="woocommerce-Input woocommerce-Input--password input-text subscribe-form-input account-form-input" placeholder="Новый пароль" name="password_1" id="password_1" autocomplete="off" />
		</div>
		</div>
		<div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
		<div class="account-input-wrapper">
			<input id="password-input" type="password" class="woocommerce-Input woocommerce-Input--password input-text subscribe-form-input account-form-input" placeholder="Подтвердить новый пароль" name="password_2" id="password_2" autocomplete="off" />
		</div>
		</div>
	<?php do_action( 'woocommerce_edit_account_form' ); ?>
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	<div class="account-button-wrapper">
		<?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
		<button type="submit" class="woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>">Изменить данные</button>
		<input type="hidden" name="action" value="save_account_details" />
	</div>
	</div>
	</div>
	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
</form>
<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
