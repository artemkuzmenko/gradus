<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 4.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

do_action( 'woocommerce_before_customer_login_form' ); ?>
<?php global $iq_gradus_options; ?>
<div class="u-columns col2-set" id="customer_login">
	<div class="u-column1">

		<h2 class="form-login-header">Авторизация</h2>
		<div class="social-login-wrapper">
			<?php echo get_ulogin_panel(); ?>
		</div>
		<form class="woocommerce-form woocommerce-form-login login" method="post">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="login-form-rows-wrapper">
						<input type="text"
							class="woocommerce-Input woocommerce-Input--text subscribe-form-input"
							name="username" id="username" placeholder="Email" autocomplete="off"
							value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" />
					</div>
				</div>
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="login-form-rows-wrapper">
						<input
							class="woocommerce-Input woocommerce-Input--text subscribe-form-input login-password-input"
							type="password" placeholder="Пароль" name="password" id="password" autocomplete="off" />
					</div>
				</div>
				<?php do_action( 'woocommerce_login_form' ); ?>
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="woocommerce-LostPassword lost_password">
						<div class="lost-password-link-wrapper">
							<a class="link-hover green-link lost-password-link"
								href="<?php echo esc_url( wp_lostpassword_url() ); ?>">Забыли пароль?</a>
						</div>
					</div>
				</div>
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="login-form-rows-wrapper">
						<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
						<button type="submit" class="woocommerce-button button woocommerce-form-login__submit"
							name="login"
							value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>"><?php esc_html_e( 'Log in', 'woocommerce' ); ?></button>
					</div>
				</div>
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="login-form-rows-wrapper">
						<div class="register-account-link-wrapper">
							<span>Нет аккаунта?</span>
							<a class="link-hover green-link register-link" data-toggle="collapse"
								href="#collapseExample" role="button" aria-expanded="false"
								aria-controls="collapseExample">
								Зарегистрируйтесь
							</a>
						</div>
					</div>
				</div>
				<?php do_action( 'woocommerce_login_form_end' ); ?>
			</div>
		</form>
	</div>
	<div class="collapse" id="collapseExample">
		<div class="u-column2 form-login-wrapper ">

			<h2 class="form-login-header register-form-header">Регистрация</h2>
			<div class="social-login-wrapper">
				<?php echo get_ulogin_panel(); ?>
			</div>
			<form method="post" class="woocommerce-form woocommerce-form-register register"
				<?php do_action( 'woocommerce_register_form_tag' ); ?>>
				<div class="row">
					<?php do_action( 'woocommerce_register_form_start' ); ?>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="login-form-rows-wrapper">
							<input type="text"
								class="woocommerce-Input woocommerce-Input--text  subscribe-form-input account-form-input"
								placeholder="Имя" name="username" id="reg_username"
								value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" />
						</div>
					</div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="login-form-rows-wrapper">
							<input type="email"
								class="woocommerce-Input woocommerce-Input--text subscribe-form-input"
								name="email" placeholder="Email" id="reg_email"
								value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" />
						</div>
					</div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="login-form-rows-wrapper">
							<input type="password"
								class="woocommerce-Input woocommerce-Input--text subscribe-form-input login-password-input"
								placeholder="Пароль" name="password" id="reg_password" />
						</div>
					</div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="login-form-rows-wrapper">
							<input id="password-input" type="password"
								class="woocommerce-Input woocommerce-Input--password subscribe-form-input login-password-input"
								placeholder="Подтвердить пароль" name="password_2" id="password_2" autocomplete="off" />
						</div>
					</div>
					<?php do_action( 'woocommerce_register_form' ); ?>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="login-form-rows-wrapper privacy-register-form-wrapper">
							Нажимая на кнопку "Зарегистрироваться", вы соглашаетесь с <br>
							<a class="link-hover green-link"
								href="<?php echo esc_url($iq_gradus_options['iq_gradus_politics']); ?>"
								target="blank">пользовательским соглашением.</a>
						</div>
					</div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="login-form-rows-wrapper">
							<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
							<button type="submit"
								class="woocommerce-Button woocommerce-button button woocommerce-form-register__submit"
								name="register"
								value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>">Зарегистрироваться</button>
						</div>
					</div>
					<?php do_action( 'woocommerce_register_form_end' ); ?>
				</div>
			</form>
		</div>
	</div>
</div>
<?php do_action( 'woocommerce_after_customer_login_form' ); ?>