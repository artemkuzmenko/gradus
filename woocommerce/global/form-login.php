<?php
/**
 * Login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( is_user_logged_in() ) {
	return;
}

?>
<div class="row">
	<form class="woocommerce-form woocommerce-form-login login checkout-form-login" method="post"
		<?php echo ( $hidden ) ? 'style="display:none;"' : ''; ?>>

		<?php do_action( 'woocommerce_login_form_start' ); ?>
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<h2 class="form-login-header checkout-form-header">Авторизация</h2>
			<div class="login-form-rows-wrapper">
				<p class="checkout-login-form-message">
					Если вы уже оформляли заказ на нашем сайте, пожалуйста, введите свои данные ниже. <br>
					Если вы являетесь новым клиентом, пожалуйста, заполните платёжные данные в соответствующем разделе.
				</p>
			</div>
		</div>
		<div class="social-login-wrapper">
			<?php echo get_ulogin_panel(); ?>
		</div>
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			<div class="login-form-rows-wrapper">
				<input type="text" class="subscribe-form-input" name="username" id="username"
					autocomplete="off" />
			</div>
		</div>
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			<div class="login-form-rows-wrapper">
				<input class="subscribe-form-input login-password-input" type="password" name="password"
					id="password" autocomplete="off" />
			</div>
		</div>
		<?php do_action( 'woocommerce_login_form' ); ?>

		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			<div class="login-form-rows-wrapper">
				<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
				<input type="hidden" name="redirect" value="<?php echo esc_url( $redirect ); ?>" />
				<button type="submit" class="woocommerce-button button woocommerce-form-login__submit" name="login"
					value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>"><?php esc_html_e( 'Login', 'woocommerce' ); ?></button>
			</div>
		</div>
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			<div class="woocommerce-LostPassword lost_password">
				<div class="lost-password-link-wrapper">
					<a class="link-hover green-link lost-password-link"
						href="<?php echo esc_url( wp_lostpassword_url() ); ?>">Забыли пароль?</a>
				</div>
			</div>
		</div>

		<?php do_action( 'woocommerce_login_form_end' ); ?>
	</form>
</div>