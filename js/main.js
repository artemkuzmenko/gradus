jQuery(function($){
    "use strict";   
    // Main page slider
    $('.main-page-slider').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: true,
        arrows: true,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 3,
              }
            },
            {
              breakpoint: 480,
              settings: {
                dots: false,
                centerMode: true,
                slidesToShow: 1
              }
            }
          ]
      });
      // Mobile menu
      var link = $('.mobile-menu-btn');
      var link_active = $('.mobile-menu-active');
      var menu = $('.mobile-menu');
      var nav_link = $('.mobile-menu a');
      link.click(function() {
        link.toggleClass('mobile-menu-active');
        menu.toggleClass('mobile-menu-active');
      });
      nav_link.click(function() {
        link.toggleClass('mobile-menu-active');
        menu.toggleClass('mobile-menu-active');
      });
      //Log in button
      var login_btn = $('.user-login-btn-wrapper');
      var login_btn_active = $('.menu-akkaunt-container-active');
      var login_menu = $('.menu-akkaunt-container');
      login_btn.click(function() {
        login_btn.toggleClass('menu-akkaunt-container-active');
        login_menu.toggleClass('menu-akkaunt-container-active');
      });
      //User length 
      $(document).ready(function() {
        var elem = $("#header-welcome");
        if(elem){
            if (elem.text().length > 10)
                    elem.text(elem.text().substr(0,10));
        }
    });
    //Heroes scroll
    $("a.scroll-to").on("click", function(e){
      e.preventDefault();
      var anchor = $(this).attr('href');
      $('html, body').animate({scrollTop: $(anchor).offset().top}, 900);
    return false;
  });
});

// Tabs script
function openTab(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
  // Get the element with id="defaultOpen" and click on it
}
// Get the element with id="defaultOpen" and click on it
window.onload = function(){
document.getElementById("defaultOpen").click();
};










