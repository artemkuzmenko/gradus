jQuery(function ($) {
    //Запускают фильтр
    $('input').on('change', function () {
        iq_gradus_get_posts();
    });
    $('.orderby').on('change', function (e) {
        e.preventDefault();
        iq_gradus_get_posts();
    });
    $(document).on("click", ".page-numbers", function (e) {
        e.preventDefault();

        var url = $(this).attr('href'); //Grab the URL destination as a string
        var paged = url.split('&paged='); //Split the string at the occurance of &paged=

        if (~url.indexOf('&paged=')) {
            paged = url.split('&paged=');
        } else {
            paged = url.split('/page/');
        }
        iq_gradus_get_posts(paged[1]); //Load Posts (feed in paged value)
    });

    //Получают данные
    function getCats() {
        var cats = []; //Setup empty array

        $(".filter-check-box:checked").each(function () {
            var val = $(this).val();
            cats.push(val); //Push value onto array
        });

        return cats; //Return all of the selected genres in an array
    }
	function getOrder() {
        var order = []; //Setup empty array

        $(".date-radio-filter:checked").each(function () {
            var val = $(this).val();
            order.push(val); //Push value onto array
        });

        return order; //Return all of the selected order value in an array
    }	

    function iq_gradus_get_posts(paged) {
        var paged_value = paged; //Store the paged value if it's being sent through when the function is called
        var ajax_url = woocommerce_params.ajax_url; //Get ajax url (added through wp_localize_script)

        $.ajax({
            type: 'GET',
            url: ajax_url,
            data: {
                action: 'iq_gradus_filter',
				order: 	getOrder,
                category: getCats,
                paged: paged_value //If paged value is being sent through with function call, store here
            },
            beforeSend: function () {
                $('#filter-container').html('Загрузка');
            },
            success: function (data) {
                //Hide loader here
                $('#filter-container').html(data);
            },
            error: function () {
                //If an ajax error has occured, do something here...
                $("#filter-container").html('<p>Ошибка, попробуйте еще раз</p>');
            }
        });
    }
});