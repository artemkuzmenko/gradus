<section class="products-slider product-card-cross-sale">
    <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="slider-info-wrapper">
                    <div class="slider-title">
                        С этой игрой также выбирают
                    </div>
                </div>
            </div>
        </div>
        <div class="main-page-slider">
        <?php
            $slider = new WP_Query( array(
            'post_type' => 'product',
            'posts_per_page' => '5',
             'orderby' => 'date',
                ));
                while ( $slider->have_posts() ): $slider->the_post(); ?>
                <div class="slide-wrapper">
                    <div class="product-card">
                        <div class="product-img">
                        <a href="<?php the_permalink(); ?>">
                        <?php if( get_field('woo_custom_image_in_slider') ): ?>
                        <img src="<?php the_field('woo_custom_image_in_slider'); ?>" />
                        <?php endif; ?>
                        </a>
                        </div>
                        <div class="product-card-info">
                            <h3 class="product-title"><a class="product-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <div class="product-card-description">
                            <a class="product-link" href="<?php the_permalink(); ?>">
                                <?php echo get_the_excerpt() ?>
                            </a>
                            </div>
                            <div class="product-price">
                            <?php woocommerce_template_loop_price(); ?>
                            </div>
                            <div class="basket-and-like-wrapper product-card-basket">
                            <?php woocommerce_template_loop_add_to_cart(); ?>
                            <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id="'.get_the_ID().'" variation_id="0"]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- / Slide end-->
                <?php endwhile; wp_reset_postdata(); ?>
        </div>
        <!-- /. Main page slider -->
    </div>
    <!-- /.container -->
</section>
<!-- / Slider section end-->