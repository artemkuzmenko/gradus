<section class="cart-steps-section">
    <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
        <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 step-3">
        <div class="steps-wrapper">
            <div class="steps-block">
            <div class="steps-block-wrapper">
            <div class="steps-block-number-wrapper">
                        01
                </div>
                <div class="step-header">
                Ваши игры
                </div>
            </div>
                <div class="steps-arrow-icon">
                <img src="<?php echo get_template_directory_uri() ?>/img/slider/grey-arrow.svg" alt="arrow grey">
                </div>
            </div>
            <div class="steps-block">
            <div class="steps-block-wrapper">
            <div class="steps-block-number-wrapper">
                        02
                </div>
                <div class="step-header">
                Оформление заказа
                </div>
            </div>
                <div class="steps-arrow-icon">
                <img src="<?php echo get_template_directory_uri() ?>/img/slider/grey-arrow.svg" alt="arrow grey">
                </div>
            </div>
            <div class="steps-block steps-block-pay">
            <div class="steps-block-wrapper">
            <div class="steps-block-number-wrapper steps-block-number-active">
                        03
                </div>
                <div class="step-header">
                Подтверждение заказа
                </div>
            </div>
            </div>
        </div>
        <!-- /.wrapper end -->
        </div>
        </div>
    </div>
</section>