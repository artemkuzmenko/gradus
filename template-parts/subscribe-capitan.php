<section class="subscribe">
    <div class="container subscribe-bg pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                <div class="subscribe-wrapper">
                    <div class="subscribe-title">
                        Подписывайтесь на наши новинки!
                    </div>
                    <div class="subscribe-subtitle">
                        Серии новых игр каждую неделю
                    </div>
                    <div class="subscribe-form">
                        <?php echo do_shortcode('[contact-form-7 id="26" title="Форма подписки"]'); ?>
                        <p class="form-info">Нажимая на кнопку "Отправить", вы соглашаетесь с <a class="green-link link-hover" href="#">пользовательским соглашением.</a></p>
                    </div>
                </div>
                <!-- / Subscribe-wrapper-->
            </div>
            <div class="col-4 hide-hero">
                <div class="hero-wrapper">
                    <img src="<?php echo get_template_directory_uri() ?>/img/heroes/Capi-new.png" alt="capitan">
                </div>
            </div>
        </div>
    </div>
</section>