<?php
/**
 * Template name: Оформление заказа
 */

get_header();
?>

<?php get_template_part('template-parts/page-title'); ?>
<?php 
if ( WC()->cart->get_cart_contents_count() > 0 ) {
	get_template_part('template-parts/cart-steps-2');
}
?>
<div class="basket-content">
    <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
            <?php
		while ( have_posts() ) :
			the_post();
			the_content();
		endwhile; // End of the loop.
		?>
    </div>
</div>
<?php get_template_part('template-parts/subscribe-capitan'); ?>

<?php get_footer(); ?>