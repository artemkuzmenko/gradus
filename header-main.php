<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package iq-gradus
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <header class="header">
        <?php global $iq_gradus_options; ?>
        <div class="container pr-0 pl-0">
            <!-- Top bar start-->
            <div class="top-bar">
                <div class="contacts-block">
                    <?php if($iq_gradus_options ['iq_gradus_phone']){ ?>
                    <div class="phone">
                        <a href="tel:<?php echo esc_attr($iq_gradus_options['iq_gradus_phone_link']); ?>"
                            class="top-bar-phone link-hover"><?php echo esc_attr($iq_gradus_options['iq_gradus_phone']); ?></a>
                    </div>
                    <?php } ?>
                    <?php if($iq_gradus_options ['iq_gradus_email']){ ?>
                    <div class="email">
                        <a href="mailto:<?php echo esc_attr($iq_gradus_options['iq_gradus_email']); ?>"
                            class="top-bar-mail link-hover"><?php echo esc_attr($iq_gradus_options['iq_gradus_email']); ?></a>
                    </div>
                    <?php } ?>
                    <?php if($iq_gradus_options ['iq_gradus_adress']){ ?>
                    <div class="adress">
                        <?php echo esc_attr($iq_gradus_options['iq_gradus_adress']); ?>
                    </div>
                    <?php } ?>
                </div>
                <div class="info-block">
                    <?php if($iq_gradus_options ['iq_gradus_politics']){ ?>
                    <div class="user-info">
                        <a href="<?php echo esc_url($iq_gradus_options['iq_gradus_politics']); ?>"
                            class="top-bar-user-info link-hover">Пользовательское соглашение</a>
                    </div>
                    <?php } ?>
                    <div class="customer-support">
                        <a href="#" class="top-bar-customer-support link-hover" data-toggle="modal" data-target="#exampleModal">Служба поддержки</a>
                    </div>
                    <div class="social-icons">
                        <?php if($iq_gradus_options ['iq_gradus_inst']){ ?>
                        <div class="social-icon-wrapper insta">
                            <a href="<?php echo esc_url($iq_gradus_options['iq_gradus_inst']); ?>"
                                class="social-icon-link"><img
                                    src="<?php echo get_template_directory_uri() ?>/img/social/inst.svg"
                                    alt="instagram"></a>
                        </div>
                        <?php } ?>
                        <?php if($iq_gradus_options ['iq_gradus_vk']){ ?>
                        <div class="social-icon-wrapper vk">
                            <a href="<?php echo esc_url($iq_gradus_options['iq_gradus_vk']); ?>"
                                class="social-icon-link"><img
                                    src="<?php echo get_template_directory_uri() ?>/img/social/vk.svg" alt="vk"></a>
                        </div>
                        <?php } ?>
                    </div>

                    <div class="call-back-btn-wrapper">
                        <button class="callback-btn" data-toggle="modal" data-target="#exampleModal">Обратная
                            связь</button>
                    </div>
                </div>
            </div>
            <!-- /.top-bar-->
        </div>
        <!-- Header nav panel start -->
        <div id="main" class="header-nav-panel">
            <div class="container header-nav-wrapper pr-0 pl-0">
                <?php if($iq_gradus_options ['iq_gradus_logo']){ ?>
                <div class="header-logo">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img
                            src="<?php echo esc_url( $iq_gradus_options['iq_gradus_logo']['url'])?>"
                            alt="header logo"></a>
                </div>
                <?php } ?>
                <!-- header-menu-wrapper start-->
                <div class="header-menu-wrapper">
                    <?php
            	wp_nav_menu( array(
                'theme_location' => 'main',
                'menu_id'        => 'main-menu',
                'container'        => 'nav',
            	) );
            	?>
                </div>
                <!-- /.header-menu-wrapper-->

                <!-- Woo block start -->
                <div class="woocommerce-block-wrapper">
                    <div class="basket">
                    <?php echo do_shortcode("[woo_cart_but]"); ?>
                    </div>
                    <?php if($iq_gradus_options ['iq_gradus_fav']){ ?>
                    <div class="favourite">
                        <a href="<?php echo esc_url($iq_gradus_options['iq_gradus_fav']); ?>"><img
                                src="<?php echo get_template_directory_uri() ?>/img/header/like.svg"
                                alt="favourite"></a>
                    </div>
                    <?php } ?>
                    <div class="user-login-btn-wrapper">
                        <?php
            	        wp_nav_menu( array(
                        'theme_location' => 'account',
                        'menu_id'        => 'account-menu',
                        'container'        => 'nav',
            	        ) );
                        ?>
                        </div>
                    <div class="mobile-menu-wrapper">
                        <span class="menu-words">
                            Меню
                        </span>
                        <div class="mobile-menu-btn">
                            <span></span>
                        </div>
                    </div>
                </div>
                <!-- /. Woo block end -->
            </div>
        </div>
        <!-- /.header nav panel end -->

        <!-- Mobile menu start -->
        <div class="mobile-menu">
            <div class="container">
                <?php
            	wp_nav_menu( array(
                'theme_location' => 'mobile',
                'menu_id'        => 'mobile-menu',
                'container'        => 'nav',
                'menu_class'        => 'mobile-menu-container',
            	) );
            	?>
                <div class="info-block">
                    <ul>
                        <?php if($iq_gradus_options ['iq_gradus_politics']){ ?>
                        <li class="mobile-menu-user-info"><a
                                href="<?php echo esc_url($iq_gradus_options['iq_gradus_politics']); ?>"
                                class="top-bar-user-info link-hover">Пользовательское соглашение</a></li>
                        <?php } ?>
                        <?php if($iq_gradus_options ['iq_gradus_support']){ ?>
                        <li><a href="<?php echo esc_url($iq_gradus_options['iq_gradus_support']); ?>"
                                class="top-bar-customer-support link-hover">Служба поддержки</a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="mobile-menu-iner-wrapper">
                    <div class="call-back-btn-wrapper">
                        <button class="callback-btn" data-toggle="modal" data-target="#exampleModal">Обратная
                            связь</button>
                    </div>
                    <div class="social-icons">
                        <?php if($iq_gradus_options ['iq_gradus_inst']){ ?>
                        <div class="social-icon-wrapper insta">
                            <a href="<?php echo esc_url($iq_gradus_options['iq_gradus_inst']); ?>"
                                class="social-icon-link"><img
                                    src="<?php echo get_template_directory_uri() ?>/img/social/inst.svg"
                                    alt="instagram"></a>
                        </div>
                        <?php } ?>
                        <?php if($iq_gradus_options ['iq_gradus_vk']){ ?>
                        <div class="social-icon-wrapper vk">
                            <a href="<?php echo esc_url($iq_gradus_options['iq_gradus_vk']); ?>"
                                class="social-icon-link"><img
                                    src="<?php echo get_template_directory_uri() ?>/img/social/vk.svg" alt="vk"></a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="contacts-block">
                    <?php if($iq_gradus_options ['iq_gradus_phone']){ ?>
                    <div class="phone">
                        <a href="tel:<?php echo esc_attr($iq_gradus_options['iq_gradus_phone_link']); ?>"
                            class="top-bar-phone link-hover"><?php echo esc_attr($iq_gradus_options['iq_gradus_phone']); ?></a>
                    </div>
                    <?php } ?>
                    <?php if($iq_gradus_options ['iq_gradus_email']){ ?>
                    <div class="email">
                        <a href="mailto:<?php echo esc_attr($iq_gradus_options['iq_gradus_email']); ?>"
                            class="top-bar-mail link-hover"><?php echo esc_attr($iq_gradus_options['iq_gradus_email']); ?></a>
                    </div>
                    <?php } ?>
                    <?php if($iq_gradus_options ['iq_gradus_adress']){ ?>
                    <div class="adress">
                        <?php echo esc_attr($iq_gradus_options['iq_gradus_adress']); ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- /.mobile menu -->
    </header>
    <!-- /.header end -->