<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @package iq-gradus
 */

?>
<?php wp_footer(); ?>
<?php global $iq_gradus_options; ?>
<footer class="footer">
        <div class="grey-footer-wrapper">
            <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
                <div class="row">
                    <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-12">
					<?php if($iq_gradus_options ['iq_gradus_logo']){ ?>
                <div class="footer-logo-wrapper">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( $iq_gradus_options['iq_gradus_logo']['url'])?>" alt="footer logo"></a>
                </div>
                <?php } ?>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <div class="nav_tabs_container">
                            <nav class="nav_tabs">
                                <div class="nav_tabs_column">
                                    <input id="footer-main-nav" type="checkbox" class="nav_tabs_input">
                                    <div class="nav_tabs_column_section">
                                        <label class="nav_tabs_column_section_label" for="footer-main-nav"><h3 class="nav_tabs_label_h">Главное</h3></label>
                                        <?php
            	                            wp_nav_menu( array(
                                            'theme_location' => 'footer-1',
                                            'menu_id'        => 'footer-menu-one',
                                             'menu_class' => 'nav_tabs_column_section_list',
            	                            ) );
            	                            ?>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <div class="nav_tabs_container">
                            <nav class="nav_tabs">
                                <div class="nav_tabs_column">
                                    <input id="footer-support-nav" type="checkbox" class="nav_tabs_input">
                                    <div class="nav_tabs_column_section">
                                        <label class="nav_tabs_column_section_label" for="footer-support-nav"><h3 class="nav_tabs_label_h">Поддержка</h3></label>
                                        <?php
            	                            wp_nav_menu( array(
                                            'theme_location' => 'footer-2',
                                            'menu_id'        => 'footer-menu-two',
                                             'menu_class' => 'nav_tabs_column_section_list',
            	                            ) );
            	                            ?>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                        <div class="footer-widget-title-wrapper">
                            <h3 class="footer-widget-title">Наши контакты</h3>
                        </div>
                        <div class="footer-contacts-wrapper">
                        <?php if($iq_gradus_options ['iq_gradus_phone']){ ?>
                            <div class="footer-phone">
                                <img class="footer-icon" src="<?php echo get_template_directory_uri() ?>/img/icons/phone.svg" alt="footer phone icon">
                                <a class="link-hover" href="tel:<?php echo esc_attr($iq_gradus_options['iq_gradus_phone_link']); ?>"><?php echo esc_attr($iq_gradus_options['iq_gradus_phone']); ?></a>
                            </div>
                            <?php } ?>
                            <?php if($iq_gradus_options ['iq_gradus_adress']){ ?>
                            <div class="footer-location">
                                <img class="footer-icon" src="<?php echo get_template_directory_uri() ?>/img/icons/location.svg" alt="footer location icon">
                                <span class="footer-adress"><?php echo esc_attr($iq_gradus_options['iq_gradus_adress']); ?></span>
                            </div>
                            <?php } ?>
                        <?php if($iq_gradus_options ['iq_gradus_email']){ ?>
                            <div class="footer-email">
                                <img class="footer-icon" src="<?php echo get_template_directory_uri() ?>/img/icons/mail.svg" alt="footer email icon">
                                <a class="link-hover" href="mailto:<?php echo esc_attr($iq_gradus_options['iq_gradus_email']); ?>"><?php echo esc_attr($iq_gradus_options['iq_gradus_email']); ?></a>
                            </div>
                            <?php } ?>
                            <div class="footer-btn-wraper">
                                <button class="callback-btn" data-toggle="modal" data-target="#exampleModal">Обратная связь</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- / row-->
            </div>
            <!-- / container-->
            <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
                <div class="bottom-footer-info">
                <?php if($iq_gradus_options ['iq_gradus_politics']){ ?>
                    <div class="user-agree">
                        <a class="link-hover user-agree-link" href="<?php echo esc_url($iq_gradus_options['iq_gradus_politics']); ?>" target="blank">Пользовательское соглашение</a>
                    </div>
                    <?php } ?>	
                    <div class="footer-social-icons-wrapper">
                        <span class="footer-social-icons-text">Социальные сети:</span>
                        <div class="social-icons">
                        <?php if($iq_gradus_options ['iq_gradus_inst']){ ?>
                            <div class="social-icon-wrapper insta">
                                <a href="<?php echo esc_url($iq_gradus_options['iq_gradus_inst']); ?>" class="social-icon-link"><img src="<?php echo get_template_directory_uri() ?>/img/social/inst.svg" alt="instagram"></a>
                            </div>
                            <?php } ?>	
                            <?php if($iq_gradus_options ['iq_gradus_vk']){ ?>
                            <div class="social-icon-wrapper vk">
                                <a href="<?php echo esc_url($iq_gradus_options['iq_gradus_vk']); ?>" class="social-icon-link"><img src="<?php echo get_template_directory_uri() ?>/img/social/vk.svg" alt="vk"></a>
                            </div>
                            <?php } ?>	
                        </div>
                    </div>
            </div>
            </div>
        </div>
        <!-- / Grey footer end-->
        <div class="container white-footer pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="we-accept">
                        <span class="we-accept-text">Мы принимаем:</span>
                        <img class="payment-icon" src="<?php echo get_template_directory_uri() ?>/img/footer/Vectormaster.svg" alt="Master card">
                        <img class="payment-icon" src="<?php echo get_template_directory_uri() ?>/img/footer/Vectorvisa.svg" alt="Visa">
                        <img class="payment-icon" src="<?php echo get_template_directory_uri() ?>/img/footer/Excludemir.svg" alt="World card">
                        <img class="payment-icon payment-icon-hide" src="<?php echo get_template_directory_uri() ?>/img/footer/Vectorapple.svg" alt="Apple pay">
                        <img class="payment-icon payment-icon-hide" src="<?php echo get_template_directory_uri() ?>/img/footer/Excludesamsung.svg" alt="Samsung pay">
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 pr-md-0 pl-md-0">
                    <div class="copyright">
                        «Интелектуальный градус» ©  <script>
                            var CurrentYear = new Date().getFullYear()
                            document.write(CurrentYear)
                        </script>                
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="developers">
                    Разработка сайта: <a class="link-hover green-link developers-link" href="https://mssg.me/olegsorokin" target="blank">Олег Сорокин</a> 
                </div>
            </div>
            </div>
        </div>
        <script src="https://yastatic.net/share2/share.js" async="async"></script>
<!-- Modal help start-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
          <?php echo do_shortcode( '[contact-form-7 id="104" title="Служба поддержки"]' ); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--Modal help end-->

<!-- Modal login start-->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container pr-xl-0 pr-lg-0 pr-md-0  pl-xl-0 pl-lg-0 pl-md-0">
          <?php echo do_shortcode( '[woocommerce_my_account]' ); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--Modal login end-->

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(65443855, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/65443855" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
    </footer>
</body>
</html>


